## Downloading GIT

In order to download this repository you will need to first download and install git.
download git from the link below:

- **https://git-scm.com/downloads**

to confirm git has been installed open a CMD window and type the following command
**git --version**
You should see the following once git has been installed properly

![image-20221209065553516](https://s2.loli.net/2022/12/09/q5Tjmy3CSNu7kOR.png)



#  Cloning the repository

In order to clone the repository you would first need to have a Gitlab.com account and have your email added to the repository.
Once you have access to the repository, proceed with the following steps:

- Click on clone and copy the HTTPS / select cloen with HTTPS
  ![image-20221209070046449](https://s2.loli.net/2022/12/09/BkryLxQu4RYSVe9.png)

- Create a folder where in your local environment where you want to place the project code base

- Inside the empty folder, right-click and select **git bash here** and it will open a terminal

  ![image-20221209070311100](https://s2.loli.net/2022/12/09/uIxFAChjVLRkEqc.png)

- In the terminal write "git clone" and add the URL you have copied earlier.
  ![image-20221209070614930](https://s2.loli.net/2022/12/09/C8IUA2mhHxTDE43.png)

- After a few seconds the terminal will run the command and the code base has now been 'cloned' in the folder you created.
  ![image-20221209070931223](https://s2.loli.net/2022/12/09/TxvI1SV4lLyCjEQ.png)
  ![image-20221209070955618](https://s2.loli.net/2022/12/09/idPlKp1x95EoC2V.png)

  Feel free to reach out if you have any questions or concerns. :')

  