<?php

ob_start();
session_start();

include("conn/connection.php");
include("conn/functions.php");

$user_data = check_login($con);

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!--BOX ICONS-->
  <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!--CSS-->
  <link rel="stylesheet" href="assets/css/styles.css">




  <title>DFCAMCLP Employee Portal</title>

  <style>
    body {
      margin-top: 4%;
    }

    .card-body {
      margin-top: 10px;
      padding: 20px;
      border-radius: 10px;
      overflow: auto;
      border-style: solid;
      border-width: 1px;
      border-color: lightgrey;
      margin-top: 2%;
    }

    .card-body-a {
      margin-top: 10px;
      padding: 20px;
      border-radius: 10px;
      border-style: solid;
      border-width: 1px;
      border-color: lightgrey;
      overflow: auto;
      height: 300px;
    }

    .card-body-b {
      margin-top: 10px;
      padding: 20px;
      border-radius: 10px;
      border-style: solid;
      border-width: 1px;
      border-color: lightgrey;
      height: auto;
    }

    .btn {

      padding: 5px 40px 5px 40px;
    }

    .button {
      text-align: center;
    }

    td {
      padding: 10px;

    }
  </style>
</head>

<body id="body-pd">
  <header class="header" id="header">
    <div class="header__toggle">
      <i class='bx bx-menu' id="header-toggle"></i>
    </div>

    <!--PROFILE ICON-->
    <div class="header__img">
      <?php echo "<b>" . $user_data['firstname'] . " " . $user_data['lastname'] . "</b>" ?>
      <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
        <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
        <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
      </svg>
    </div>
  </header>

  <div class="l-navbar" id="nav-bar">
    <nav class="nav">
      <div>
        <a href="index.php" class="nav__logo">
          <i class='bx bx-layer nav__logo-icon'></i>
          <span class="nav__logo-name">DFCAMCLP Portal</span>
        </a>

        <div class="nav__list">
          <a href="index.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-house-door" viewBox="0 0 16 16">
              <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z" />
            </svg>
            <span class="nav__name">Home</span>
          </a>

          <a href="profile.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
              <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
            </svg>
            <span class="nav__name">Profile</span>
          </a>

          <a href="attendance.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-calendar-date" viewBox="0 0 16 16">
              <path d="M6.445 11.688V6.354h-.633A12.6 12.6 0 0 0 4.5 7.16v.695c.375-.257.969-.62 1.258-.777h.012v4.61h.675zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871 0-1.934-.781-2.668-1.953-2.668-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82h-.684zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23z" />
              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
            </svg>
            <span class="nav__name">Attendance</span>
          </a>

          <a href="#" class="nav__link active">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-calendar-event" viewBox="0 0 16 16">
              <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z" />
              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
            </svg>
            <span class="nav__name">Leave</span>
          </a>

          <a href="payslip.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-wallet-fill" viewBox="0 0 16 16">
              <path d="M1.5 2A1.5 1.5 0 0 0 0 3.5v2h6a.5.5 0 0 1 .5.5c0 .253.08.644.306.958.207.288.557.542 1.194.542.637 0 .987-.254 1.194-.542.226-.314.306-.705.306-.958a.5.5 0 0 1 .5-.5h6v-2A1.5 1.5 0 0 0 14.5 2h-13z" />
              <path d="M16 6.5h-5.551a2.678 2.678 0 0 1-.443 1.042C9.613 8.088 8.963 8.5 8 8.5c-.963 0-1.613-.412-2.006-.958A2.679 2.679 0 0 1 5.551 6.5H0v6A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-6z" />
            </svg>
            <span class="nav__name">Payslip</span>
          </a>
        </div>
      </div>

      <a href="logout.php" class="nav__link">
        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-box-arrow-left" viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z" />
          <path fill-rule="evenodd" d="M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z" />
        </svg>
        <span class="nav__name">Log Out</span>
      </a>
    </nav>
  </div>

  <div class="card-body text-center">
    <div class="FormContent">
      <form method="post">
        <!--name will be based on the saved database-->
        <h2 style="text-align: center;">Employee Leave Request Form</h2>
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td> <label for="fname">First name:</label> </td>
              <td> <input type="text" id="fname" name="fname" value="<?php echo $user_data['firstname'] ?>" readonly> </td>
              <td> <label for="lname">Last name:</label> </td>
              <td> <input type="text" id="lname" name="lname" value="<?php echo $user_data['lastname'] ?>" readonly> </td>
            </tr>
            <tr>
              <td> <label for="empId">Employee ID:</label> </td>
              <td> <input type="text" id="empId" name="empId" value="<?php echo $user_data['empId'] ?>" readonly> </td>
              <td> <label for="JobTitle">Job Title: </label> </td>
              <td> <input type="text" id="JobTitle" name="JobTitle" value="<?php echo $user_data['role'] ?>" readonly> </td>
            </tr>
            <tr>
              <td> <label for="LeaveDate">Leave Date: </label> </td>
              <td> <input type="date" id="LeaveDate" name="LeaveDate" required=""> </td>
              <td> <label for="LeaveType">Leave Type: </label> </td>
              <td> <select class value="" name="leave_duration" required="">
                  <option></option>
                  <option value="Sick Leave">Sick Leave</option>
                  <option value="Vacation Leave">Vacation Leave</option>
                  <option value="Special Privilege Leave">Special Privilege Leave</option>
                </select> </td>
            </tr>
            <tr>
              <td colspan="4" style="text-align: center;">
                <input type="submit" name="submit" class="btn btn-outline-primary ms-1" value="Submit" style="float:right;">
                <!--<a href="leave_balance.php" style="float: left;">
                <button type="button" class="btn btn-outline-primary ms-1">Leave Balance</button>
                </a>-->
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
  </div>

  <?php

  $empId = $user_data['empId'];

  // write query for all requests
  $sql = "SELECT * FROM leaverequest WHERE empId = $empId";

  // make query and get result
  $result = mysqli_query($con, $sql);

  // fetch the resulting rows as an array
  $requests = mysqli_fetch_all($result, MYSQLI_ASSOC);

  // free result from memory
  mysqli_free_result($result);

  // close connection

  ?>

  <div class="card-body-b">
    <h2 style="text-align: center;">Submitted Leave Request</h2>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th scope="col">Employee ID</th>
          <th scope="col">Name</th>
          <th scope="col">Leave Type</th>
          <th scope="col">Leave Date</th>
          <th scope="col">Status</th>
        </tr>
      </thead>
      <?php foreach ($requests as $request) : ?>
        <tbody>
          <tr>
            <td><?php echo $request['empId'] ?></td>
            <td><?php echo $request['firstname'] . " " . $request['lastname'] ?></td>
            <td><?php echo $request['leaveType'] ?></td>
            <td><?php echo $request['date'] ?></td>
            <td><?php echo $request['status'] ?></td>
          </tr>
        </tbody>
      <?php endforeach; ?>
    </table>
  </div>

  <?php

  $empId = $user_data['empId'];

  // write query for all requests
  $sql = "SELECT * FROM approved WHERE empId = $empId";

  // make query and get result
  $result = mysqli_query($con, $sql);

  // fetch the resulting rows as an array
  $approved = mysqli_fetch_all($result, MYSQLI_ASSOC);

  // free result from memory
  mysqli_free_result($result);

  // close connection

  ?>

  <?php

  $empId = $user_data['empId'];

  // write query for all requests
  $sql = "SELECT * FROM declined WHERE empId = $empId";

  // make query and get result
  $result = mysqli_query($con, $sql);

  // fetch the resulting rows as an array
  $declined = mysqli_fetch_all($result, MYSQLI_ASSOC);

  // free result from memory
  mysqli_free_result($result);

  // close connection

  ?>

  <div class="row">
    <div class="col">
      <div class="card-body-a">
        <h2 style="text-align: center;">Approved Leave Requests</h2>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col">Employee ID</th>
              <th scope="col">Name</th>
              <th scope="col">Leave Type</th>
              <th scope="col">Leave Date</th>
            </tr>
          </thead>
          <?php foreach ($approved as $approve) : ?>
            <tbody>
              <tr>
                <td>
                  <p><?php echo $approve['empId'] ?></p>
                </td>
                <td>
                  <p><?php echo $approve['firstname'] . " " . $approve['lastname'] ?></p>
                </td>
                <td>
                  <p><?php echo $approve['leaveType'] ?></p>
                </td>
                <td>
                  <p><?php echo $approve['date'] ?></p>
                </td>
              </tr>
            </tbody>
          <?php endforeach; ?>
        </table>

      </div>
    </div>
    <div class="col">
      <div class="card-body-a">
        <h2 style="text-align: center;">Declined Leave Requests</h2>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col">Employee ID</th>
              <th scope="col">Name</th>
              <th scope="col">Leave Type</th>
              <th scope="col">Leave Date</th>
            </tr>
          </thead>
          <?php foreach ($declined as $decline) : ?>
            <tbody>
              <tr>
                <td>
                  <p><?php echo $decline['empId'] ?></p>
                </td>
                <td>
                  <p><?php echo $decline['firstname'] . " " . $decline['lastname'] ?></p>
                </td>
                <td>
                  <p><?php echo $decline['leaveType'] ?></p>
                </td>
                <td>
                  <p><?php echo $decline['date'] ?></p>
                </td>
              </tr>
            </tbody>
          <?php endforeach; ?>
        </table>
      </div>
    </div>
  </div>

  <?php

  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    // something was posted
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $empId = $_POST['empId'];
    $role = $_POST['JobTitle'];
    $date = $_POST['LeaveDate'];
    $type = $_POST['leave_duration'];
    $status = "PENDING";

    $leave_date = $request['date'];



    if (isset($_POST['submit'])) {
      if ($date == $leave_date) {
        echo '<script>alert("Cannot file multiple requests!")</script>';
      } else {
        $appUpdateQuery = "DELETE FROM leaverequest WHERE `leaverequest`.`empId` = $empId";
        $appUpdateResult = mysqli_query($con, $appUpdateQuery);
        $sql = "insert into leaverequest (empId,firstname,lastname,role,leaveType,date,status) values ('$empId','$fname','$lname','$role','$type','$date','$status')";
        mysqli_query($con, $sql);
        header('Location: leave.php');
      }
    }
  }

  ?>

  <!--===== MAIN JS =====-->
  <script src="assets/js/main.js"></script>

</body>

</html>