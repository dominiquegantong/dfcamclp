<?php

session_start();

include("conn/connection.php");
include("conn/functions.php");

?>

<!DOCTYPE html>
<html>

<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
</head>

<body>
    <div class="bg"></div>
    <div class="loginbox">
        <img src="img/avatars.png" class="avatar" />
        <h1>Forgot Password</h1>
        <form method="post" action="" autocomplete="off">
            <p>Email</p>
            <input type="text" name="email" placeholder="Enter Email" />

            <input type="submit" name="" value="Submit" />
            <a href="Login.php">Log In</a><br />
        </form>
    </div>
    <?php

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        //something was posted
        $email = $_POST['email'];

        //read from database
        $query = "select * from profile where email = '$email'";
        $result = mysqli_query($con, $query);

        if ($result) {
            if ($result && mysqli_num_rows($result) > 0) {

                $user_data = mysqli_fetch_assoc($result);

                if ($user_data['email'] === $email) {

                    $_SESSION['id'] = $user_data['id'];
                    header("Location: confirmID.php");
                    die;
                }
            }
        }

        if (!empty($email)) {
            echo '<script>alert("Email is incorrect or not registered!");</script>';
        }
    }

    ?>
    <script src="script/script.js"></script>
    <script src="script/script2.js"></script>
</body>
</head>

</html>