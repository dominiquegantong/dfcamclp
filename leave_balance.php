<?php

/*
 *
 *
 * 
 *
 * !! ~ READ ME ~ !!
 * 
 * DISREGARD THIS PAGE.
 * 
 * NOT NEEDED.
 * 
 * 
 * 
 * 
 */

session_start();

include("conn/connection.php");
include("conn/functions.php");

$user_data = check_login($con);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>DFCAMCLP Employee Portal</title>
    <style>
        html {
            font-family: arial;
        }

        body {
            margin: 0;
            position: relative;
        }

        header {
            padding: 10px 10px 10px 15px;
            background-color: #f7f6fb;
            overflow: hidden;
        }

        button {
            background-color: #212529;
            width: 80px;
            height: 40px;
            border-style: none;
            border-radius: 5px;
            cursor: pointer;
            color: white;
        }

        button:hover {
            opacity: 0.7;
        }

        .card-body-a {
            margin: 10px 40vh 0 40vh;
            padding: 20px;
            border-radius: 10px;
            border-style: solid;
            border-width: 1px;
            border-color: lightgrey;
            overflow: auto;
            height: 280px;
            box-shadow: 0 3px 5px rgb(0 0 0 / 0.2);
        }
    </style>
</head>

<body>
    <header>
        <a href="leave.php"><button>Back</button></a>
    </header>

    <?php

    // write query for all requests
    $sql = "SELECT * FROM `sick_leave` ORDER BY id";

    // make query and get result
    $result = mysqli_query($con, $sql);

    // fetch the resulting rows as an array
    $sickLeave = mysqli_fetch_all($result, MYSQLI_ASSOC);

    // free result from memory
    mysqli_free_result($result);

    // close connection

    ?>

    <div class="col">
        <div class="col">
            <div class="card-body-a">
                <h2 style="text-align: center;">Sick Leave Balance</h2>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Employee ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Leave Date</th>
                        </tr>
                    </thead>
                    <?php foreach ($sickLeave as $sick) : ?>
                        <tbody>
                            <tr>
                                <td>
                                    <p><?php echo $user_data['empId'] ?></p>
                                </td>
                                <td>
                                    <p><?php echo $user_data['firstname'] . " " . $user_data['lastname'] ?></p>
                                </td>
                                <td>
                                    <p><?php echo $sick['date'] ?></p>
                                </td>
                            </tr>
                        </tbody>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>

        <?php

        // write query for all requests
        $sql = "SELECT * FROM `vacation_leave` ORDER BY id";

        // make query and get result
        $result = mysqli_query($con, $sql);

        // fetch the resulting rows as an array
        $vacationLeave = mysqli_fetch_all($result, MYSQLI_ASSOC);

        // free result from memory
        mysqli_free_result($result);

        // close connection

        ?>

        <div class=" col">
            <div class="card-body-a">
                <h2 style="text-align: center;">Vacation Leave Balance</h2>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Employee ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Leave Date</th>
                        </tr>
                    </thead>
                    <?php foreach ($vacationLeave as $vacation) : ?>
                        <tbody>
                            <tr>
                                <td>
                                    <p><?php echo $user_data['empId'] ?></p>
                                </td>
                                <td>
                                    <p><?php echo $user_data['firstname'] . " " . $user_data['lastname'] ?></p>
                                </td>
                                <td>
                                    <p><?php echo $vacation['date'] ?></p>
                                </td>
                            </tr>
                        </tbody>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>

        <?php

        $empId = $user_data['empId'];

        // write query for all requests
        $sql = "SELECT * FROM `special_privilege_leave` ORDER BY id";

        // make query and get result
        $result = mysqli_query($con, $sql);

        // fetch the resulting rows as an array
        $specialPrivilegeLeave = mysqli_fetch_all($result, MYSQLI_ASSOC);

        // free result from memory
        mysqli_free_result($result);

        // close connection

        ?>

        <div class="col">
            <div class="card-body-a">
                <h2 style="text-align: center;">Special Privilege Leave Balance</h2>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Employee ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Leave Date</th>
                        </tr>
                    </thead>
                    <?php foreach ($specialPrivilegeLeave as $SPL) : ?>
                        <tbody>
                            <tr>
                                <td>
                                    <p><?php echo $user_data['empId'] ?></p>
                                </td>
                                <td>
                                    <p><?php echo $user_data['firstname'] . " " . $user_data['lastname'] ?></p>
                                </td>
                                <td>
                                    <p><?php echo $SPL['date'] ?></p>
                                </td>
                            </tr>
                        </tbody>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</body>

</html>