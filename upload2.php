
<?php  
session_start();

if (isset($_SESSION['id'])) {



if(isset($_POST['firstname'])){

    include("conn/connection.php");

    $fname = $_POST['firstname'];
    $id = $_SESSION['id'];
    
      if (isset($_FILES['image']['name']) AND !empty($_FILES['image']['name'])) {
         
        
         $img_name = $_FILES['image']['name'];
         $tmp_name = $_FILES['image']['tmp_name'];
         $error = $_FILES['image']['error'];
         
         if($error === 0){
            $img_ex = pathinfo($img_name, PATHINFO_EXTENSION);
            $img_ex_to_lc = strtolower($img_ex);

            $allowed_exs = array('jpg', 'jpeg', 'png');
            if(in_array($img_ex_to_lc, $allowed_exs)){
               $new_img_name = uniqid($uname, true).'.'.$img_ex_to_lc;
               $img_upload_path = '../upload/'.$new_img_name;
               // Delete old profile pic
               $old_pp_des = "../upload/$old_pp";
               if(unlink($old_pp_des)){
                  // just deleted
                  move_uploaded_file($tmp_name, $img_upload_path);
               }else {
                  // error or already deleted
                  move_uploaded_file($tmp_name, $img_upload_path);
               }
               

               // update the Database
               $sql = "UPDATE profile 
                       SET firstname=?, pp=?
                       WHERE id=?";
               $stmt = $con->prepare($sql);
               $stmt->execute([$fname, $new_img_name, $id]);
               $_SESSION['firstname'] = $fname;
               header("Location: ../profile.php?success=Your account has been updated successfully");
                exit;
            }else {
               $em = "You can't upload files of this type";
               header("Location: ../profile.php?error=$em&$data");
               exit;
            }
         }else {
            $em = "unknown error occurred!";
            header("Location: ../profile.php?error=$em&$data");
            exit;
         }

        
      }else {
        $sql = "UPDATE profile 
                SET firstname=?
                WHERE id=?";
        $stmt = $con->prepare($sql);
        $stmt->execute([$fname, $id]);

        header("Location: ../profile.php?success=Your account has been updated successfully");
        exit;
      }
    


}else {
  header("Location: ../profile.php?error=error");
  exit;
}


}else {
  header("Location: profile.php");
  exit;
} 

