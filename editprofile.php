<?php
ob_start();
session_start();

include("conn/connection.php");
include("conn/functions.php");

$user_data = check_login($con);

?>

<?php
        $id = $user_data["id"];
        $name = $user_data["firstname"];
        $image = $user_data["image"];
        ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!--BOX ICONS-->
  <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">


  <!--CSS-->
  <link rel="stylesheet" href="assets/css/styles.css">

  <title>DFCAMCLP Employee Portal</title>
</head>

<body id="body-pd">
  <header class="header" id="header">
    <div class="header__toggle">
      <i class='bx bx-menu' id="header-toggle"></i>
    </div>
    <div class="header__img">
      <?php echo "<b>" . $user_data['firstname'] . " " . $user_data['lastname'] . "</b>" ?>
      <img src="upload/<?php echo $image; ?>" width = 35 height = 35 title="<?php echo $image; ?>">
    </div>
  </header>

  <div class="l-navbar" id="nav-bar">
    <nav class="nav">
      <div>
        <a href="index.php" class="nav__logo">
          <i class='bx bx-layer nav__logo-icon'></i>
          <span class="nav__logo-name">DFCAMCLP Portal</span>
        </a>

        <div class="nav__list">
          <a href="index.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-house-door" viewBox="0 0 16 16">
              <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z" />
            </svg>
            <span class="nav__name">Home</span>
          </a>

          <a href="profile.php" class="nav__link active">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
              <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
            </svg>
            <span class="nav__name">Profile</span>
          </a>

          <a href="attendance.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-calendar-date" viewBox="0 0 16 16">
              <path d="M6.445 11.688V6.354h-.633A12.6 12.6 0 0 0 4.5 7.16v.695c.375-.257.969-.62 1.258-.777h.012v4.61h.675zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871 0-1.934-.781-2.668-1.953-2.668-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82h-.684zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23z" />
              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
            </svg>
            <span class="nav__name">Attendance</span>
          </a>

          <a href="leave.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-calendar-event" viewBox="0 0 16 16">
              <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z" />
              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
            </svg>
            <span class="nav__name">Leave</span>
          </a>

          <a href="payslip.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-wallet-fill" viewBox="0 0 16 16">
              <path d="M1.5 2A1.5 1.5 0 0 0 0 3.5v2h6a.5.5 0 0 1 .5.5c0 .253.08.644.306.958.207.288.557.542 1.194.542.637 0 .987-.254 1.194-.542.226-.314.306-.705.306-.958a.5.5 0 0 1 .5-.5h6v-2A1.5 1.5 0 0 0 14.5 2h-13z" />
              <path d="M16 6.5h-5.551a2.678 2.678 0 0 1-.443 1.042C9.613 8.088 8.963 8.5 8 8.5c-.963 0-1.613-.412-2.006-.958A2.679 2.679 0 0 1 5.551 6.5H0v6A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-6z" />
            </svg>
            <span class="nav__name">Payslip</span>
          </a>
        </div>
      </div>

      <a href="logout.php" class="nav__link">
        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-box-arrow-left" viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z" />
          <path fill-rule="evenodd" d="M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z" />
        </svg>
        <span class="nav__name">Log Out</span>
      </a>
    </nav>
  </div>

  <!--CONTENT-->

  <form method="post" onsubmit="return alert(' requiredProfile successfully updated!');">
    <div class="main" id="main">

      <div class="row">
        <div class="col-lg-4">
          <div class="card mb-4">
            <div class="card-body text-center">
             <img src="upload/<?php echo $image; ?>" width = 125 height = 125 title="<?php echo $image; ?>">
              <h5 class="my-3"><?php echo $user_data['firstname'] . " " . $user_data['lastname'] ?></h5>
              <p class="text-muted mb-1"><b>ROLE:</b> <?php echo $user_data['role']; ?></p>
              <p>ID: <?php echo $user_data['empId'] ?></p>
              <div class="d-flex justify-content-center mb-2">
                <a href="profile.php">
                  <button type="button" class="btn btn-outline-primary ms-1">Back</button>
                </a>
                <a href="changepass.php?id=<?php echo $user_data['id'] ?>">
                  <button type="button" class="btn btn-outline-primary ms-1">Change Password</button>
                </a>
                <input id="btnUpdate" type="submit" name="submit" class="btn btn-outline-primary ms-1" value="Update Profile" onclick="return message();">
              </div>
            </div>
          </div>

          <div class="col-lg-12">
          <div class="card mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">GSIS Number</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $user_data['gsis']; ?></p>
                </div>
              </div>
     
              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">TIN</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $user_data['tin']; ?></p>
                </div>
              </div>

              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">SSS</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $user_data['sss']; ?></p>
                </div>
              </div>
              <hr>

              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">PhilHealth</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $user_data['philhealth']; ?></p>
                </div>
              </div>

              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">Pag-Ibig</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $user_data['pagibig']; ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        </div>
        <div class="col-lg-8">
          <div class="card mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">First Name</p>
                </div>
                <div class="col-sm-9">
                  <form class="form" action="upload.php?login_id=<?php echo $login_id?>" method="POST" enctype="multipart/form-data">
                Select image to upload:
                <div class="form-group">
                   <input type="file" name="fileToUpload" id="fileToUpload">
                </div>
              </form>
              <a class="user-link" href="#">
                  <img class="media-object img-thumbnail user-img" alt="User Picture" src=<?php echo $user_data['image']; ?>>
              </a>

                </div>
              </div>

              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">First Name</p>
                </div>
                <div class="col-sm-9">
                  <p class="text-muted mb-0"><?php echo $user_data['firstname']; ?></p>
                </div>
              </div>

              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Middle Name</p>
                </div>
                <div class="col-sm-9">
                  <p class="text-muted mb-0"><?php echo $user_data['middlename'] ?></p>
                </div>
              </div>

              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Last Name</p>
                </div>
                <div class="col-sm-9">
                  <p class="text-muted mb-0"><?php echo $user_data['lastname'] ?></p>
                </div>
              </div>

              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Date of Birth</p>
                </div>
                <div class="col-sm-9">
                  <input type="date" name="DoB">
                  <p class="text-muted mb-0"><?php echo $user_data['birthdate'] ?></p>
                </div>
              </div>

              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Age</p>
                </div>
                <div class="col-sm-9">
                  <input type="number" name="age" placeholder="Age" autocomplete="off">
                </div>
              </div>

              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Gender</p>
                </div>
                <div class="col-sm-9">
                  <select class="gender" value="" name="gender" readonly="" required>
                    <option></option>
                    <option value="N/A">N/A</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                  </select>
                </div>
              </div>

              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Marital Status</p>
                </div>
                <div class="col-sm-9">
                  <select class="status" value="" name="maritalStatus" readonly="">
                    <option></option>
                    <option value="N/A">N/A</option>
                    <option value="Single">Single</option>
                    <option value="Married">Married</option>
                    <option value="Divorced">Divorced</option>
                    <option value="Separated">Separated</option>
                    <option value="Widowed">Widowed</option>
                  </select>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Email Address</p>
                </div>
                <div class="col-sm-9">
                  <input type="email" name="email" placeholder="Email Address" autocomplete="off" required>
                </div>
              </div>
              <hr>

              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Phone</p>
                </div>
                <div class="col-sm-9">
                  <input type="number" name="phone" placeholder="Phone Number" autocomplete="off">
                </div>
              </div>
              <hr>

              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Mobile</p>
                </div>
                <div class="col-sm-9">
                  <input type="number" name="mobile" placeholder="Mobile Number" autocomplete="off">
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Address</p>
                </div>
                <div class="col-sm-9">
                  <input type="text" name="address" placeholder="Address" autocomplete="off">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
  </div>

  </div>

  <?php
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = $user_data['email'];

    if (isset($_POST['submit'])) {
      if (empty($_POST['DoB'])) {
      } else {
        $queryDoB = "update profile set birthdate='" . $_POST['DoB'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryDoB);
      }
      if (empty($_POST['age'])) {
      } else {
        $queryAge = "update profile set age='" . $_POST['age'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryAge);
      }
      if (empty($_POST['gender'])) {
      } else {
        $queryGender = "update profile set gender='" . $_POST['gender'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryGender);
      }
      if (empty($_POST['maritalStatus'])) {
      } else {
        $queryMaritalStatus = "update profile set maritalstatus='" . $_POST['maritalStatus'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryMaritalStatus);
      }
      if (empty($_POST['email'])) {
      } else {
        $queryEmail = "update profile set email='" . $_POST['email'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryEmail);
      }
      if (empty($_POST['phone'])) {
      } else {
        $queryPhone = "update profile set phone='" . $_POST['phone'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryPhone);
      }
      if (empty($_POST['mobile'])) {
      } else {
        $queryMobile = "update profile set mobile='" . $_POST['mobile'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryMobile);
      }
      if (empty($_POST['address'])) {
      } else {
        $queryAddress = "update profile set address='" . $_POST['address'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryAddress);
      }
      if (empty($_POST['gsis'])) {
      } else {
        $queryGsis = "update profile set gsis='" . $_POST['gsis'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryGsis);
      }
      if (empty($_POST['tin'])) {
      } else {
        $queryTin = "update profile set tin='" . $_POST['tin'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryTin);
      }
      if (empty($_POST['sss'])) {
      } else {
        $querySss = "update profile set sss='" . $_POST['sss'] . "' where email='" . $email . "'";
        mysqli_query($con, $querySss);
      }
      if (empty($_POST['image'])) {
      } else {
        $queryimages = "update profile set images='" . $_POST['images'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryimages);
      }
      if (empty($_POST['philhealth'])) {
      } else {
        $queryPhilHealth = "update profile set philhealth='" . $_POST['philhealth'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryPhilHealth);
      }
      if (empty($_POST['pagibig'])) {
      } else {
        $queryPagIbig = "update profile set pagibig='" . $_POST['pagibig'] . "' where email='" . $email . "'";
        mysqli_query($con, $queryPagIbig);
      }
      echo '<script>alert("Profile successfully updated!")</script>';
      header("Location: profile.php");
    }
  }

  ?>
  <!--===== MAIN JS =====-->
  <script src="assets/js/main.js"></script>
</body>

</html>