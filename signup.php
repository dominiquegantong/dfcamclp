<?php

session_start();

include("conn/connection.php");
include("conn/functions.php");

// import email class
require_once dirname(__FILE__) . '/classes/EmailClass.php';
//start import of twilio
require_once __DIR__ . '\classes\twilio-php-main\src\Twilio\autoload.php';
use Twilio\Rest\Client; //This part uses the Client class under twilio
$sid    = "AC28b3232b238fce9a2167f0eceda76098"; //Twilio account SID
$token  = "ead904d3a10f3d5569335c0010a8ae3b";  //Twilio account Token.
$twilio = new Client($sid, $token); //declare twilio variable as new client for later use
?>

<!DOCTYPE html>
<html>

<head>
  <title>Sign Up</title>
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" />
</head>

<body>
  <div class="bg"></div>
  <div class="loginbox">
    <img src="img/avatars.png" class="avatar" />
    <h1>SIGN UP</h1>
    <form method="post" name="form1">
      <input type="text" name="firstname" placeholder="Firstname" autocomplete="off" />
      <input type="text" name="middlename" placeholder="Middlename" autocomplete="off" />
      <input type="text" name="lastname" placeholder="Lastname" />
      <input type="text" name="email" placeholder="Email" autocomplete="off" pattern= "[a-zA-Z][a-zA-Z0-9-_.]+@dfcamclp.edu.ph" />
      <input id="id_password" type="password" name="password" placeholder="Password" autocomplete="off" />
      <i class="far fa-eye" id="togglePassword" style="margin-left: -30px; cursor: pointer"></i>
      <input id="id_password2" type="password" name="confirmpassword" placeholder="Confirm Password" />
      <i class="far fa-eye" id="togglePassword2" style="margin-left: -30px; cursor: pointer"></i>
      <input type="submit" name="" value="Sign Up" />
      <a href="login.php">Already have an account</a>
    </form>
  </div>
  <?php

  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    //something was posted
    $empId = rand(1000000, 9999999);
    $firstname = $_POST['firstname'];
    $middlename = $_POST['middlename'];
    $lastname = $_POST['lastname'];
    $email = ($_POST['email']);
    $password = $_POST['password'];
    $confirmpassword = $_POST['confirmpassword'];
    $role = "Instructor";

    if (!empty($firstname) && !empty($middlename) && !empty($lastname) && !empty($email) && !empty($password) && !empty($confirmpassword) && !is_numeric($email)) {

      //save to database
      $query = "insert into profile (empId,firstname,middlename,lastname,role,email,password) values ('$empId','$firstname','$middlename','$lastname','$role','$email','$password')";

      //check if username is taken
      $select = mysqli_query($con, "select * from profile where email = '" . $_POST['email'] . "'");

      if (mysqli_num_rows($select)) {
        echo '<script>alert("Email is already taken!")</script>';
      } else if ($_POST['password'] != $_POST['confirmpassword']) {
        echo '<script>alert("Password confirmation did not match!")</script>';
      } else {
        echo '<script>alert("Account successfully created!")</script>';
        $insertIntoProfile = mysqli_query($con, $query);
        header("Location: login.php");

        //-------------------- email notification ---------------------------

        $data = array(
          'recipient' => $_POST['email'],
          'subject'   => "DFCAMCLP SATELLITE CAMPUS - WELCOME, $firstname!",
          'body'      => "

          <p>Good day! Here are your credentials.</p>
          <p>Make sure to keep your account secured to avoid any future problems.</p>
          <p>Thank you!</p>

          <br>

          <p><b>Employee ID:</b> <i>$empId</i></p>
          <p><b>Password:</b> <i>$password</i></p>
          "
        );

        $email_class  = new EmailClass($data);

        $notification = $email_class->notification();
        //---- This section starts the twilio SMS API ----
        $message = $twilio->messages->create( //Create a message and use the $twilio variable we set above
          '+639152857371', // Recipient of SMS notification
          [
            'from' => '+19498326678', // Twilio number provided from Twilio.com
            'body' => 'sample sms from sign up' // duh
          ]
        );
        //---- This section ends ----
        //-------------------- end of email notification --------------------

        die;
      }
    } else {
      echo '<script>alert("Please input all fields!")</script>';
    }
  }

  ?>
  <script src="script/script.js"></script>
  <script src="script/script2.js"></script>
</body>

</html>