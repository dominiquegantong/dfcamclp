<?php

session_start();

include("conn/connection.php");
include("conn/functions.php");

$user_data = check_login($con);

?>

<?php
        $id = $user_data["id"];
        $name = $user_data["firstname"];
        $image = $user_data["image"];
        ?>

<?php
    if(isset($_FILES["image"]["name"])){
      $id = $_POST["id"];
      $name = $_POST["firstname"];

      $imageName = $_FILES["image"]["name"];
      $imageSize = $_FILES["image"]["size"];
      $tmpName = $_FILES["image"]["tmp_name"];

      // Image validation
      $validImageExtension = ['jpg', 'jpeg', 'png'];
      $imageExtension = explode('.', $imageName);
      $imageExtension = strtolower(end($imageExtension));
      if (!in_array($imageExtension, $validImageExtension)){
        echo
        "
        <script>
          alert('Invalid Image Extension');
          document.location.href = '../dfcamclp/profile.php';
        </script>
        ";
      }
      elseif ($imageSize > 1200000){
        echo
        "
        <script>
          alert('Image Size Is Too Large');
          document.location.href = '../dfcamclp/profile.php';
        </script>
        ";
      }
      else{
        $newImageName = $name . " - " . date("Y.m.d") . " - " . date("h.i.sa"); // Generate new image name
        $newImageName .= '.' . $imageExtension;
        $query = "UPDATE profile SET image = '$newImageName' WHERE id = $id";
        mysqli_query($con, $query);
        move_uploaded_file($tmpName, 'upload/' . $newImageName);
        echo
        "
        <script>
        document.location.href = '../dfcamclp/profile.php';
        </script>
        ";
      }
    }
    ?>



<!DOCTYPE html>
<html lang="en">

<head>
  <style>
        :root {
            --modal-duration: 1s;
            --modal-color: #428bca;
        }



        .modal {
            display: none;
            position: fixed;
            z-index: 1;
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.5);
        }

        .modal-content {
            margin: 10% auto;
            width: 60%;
            box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 7px 20px 0 rgba(0, 0, 0, 0.17);
            animation-name: modalopen;
            animation-duration: var(--modal-duration);
        }

        .modal-header h2,
        .modal-footer h3 {
            margin: 0;
        }

        .modal-header {
            background: var(--modal-color);
            padding: 15px;
            color: #fff;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        .modal-body {
            padding: 10px 20px;
            background: #fff;
        }

        .modal-footer {
            background: var(--modal-color);
            padding: 10px;
            color: #fff;
            text-align: center;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
        }

        .close {
            color: #ccc;
            float: right;
            font-size: 30px;
            color: #fff;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        @keyframes modalopen {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }
    </style>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <!--BOX ICONS-->
  <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">


  <!--CSS-->
  <link rel="stylesheet" href="assets/css/styles.css">

  <title>DFCAMCLP Employee Portal</title>
</head>

<body id="body-pd">



  <header class="header" id="header">
    <div class="header__toggle">
      <i class='bx bx-menu' id="header-toggle"></i>
    </div>

    <!--PROFILE ICON-->
    <div class="header__img">
      <?php echo "<b>" . $user_data['firstname'] . " " . $user_data['lastname'] . "</b>" ?>
      <img src="upload/<?php echo $image; ?>" width = 35 height = 35 title="<?php echo $image; ?>">
    </div>
  </header>

  <div class="l-navbar" id="nav-bar">
    <nav class="nav">
      <div>
        <a href="index.php" class="nav__logo">
          <i class='bx bx-layer nav__logo-icon'></i>
          <span class="nav__logo-name">DFCAMCLP Portal</span>
        </a>

        <div class="nav__list">
          <a href="index.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-house-door" viewBox="0 0 16 16">
              <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z" />
            </svg>
            <span class="nav__name">Home</span>
          </a>

          <a href="#" class="nav__link active">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
              <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
            </svg>
            <span class="nav__name">Profile</span>
          </a>

          <a href="attendance.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-calendar-date" viewBox="0 0 16 16">
              <path d="M6.445 11.688V6.354h-.633A12.6 12.6 0 0 0 4.5 7.16v.695c.375-.257.969-.62 1.258-.777h.012v4.61h.675zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871 0-1.934-.781-2.668-1.953-2.668-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82h-.684zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23z" />
              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
            </svg>
            <span class="nav__name">Attendance</span>
          </a>

          <a href="leave.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-calendar-event" viewBox="0 0 16 16">
              <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z" />
              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
            </svg>
            <span class="nav__name">Leave</span>
          </a>

          <a href="payslip.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-wallet-fill" viewBox="0 0 16 16">
              <path d="M1.5 2A1.5 1.5 0 0 0 0 3.5v2h6a.5.5 0 0 1 .5.5c0 .253.08.644.306.958.207.288.557.542 1.194.542.637 0 .987-.254 1.194-.542.226-.314.306-.705.306-.958a.5.5 0 0 1 .5-.5h6v-2A1.5 1.5 0 0 0 14.5 2h-13z" />
              <path d="M16 6.5h-5.551a2.678 2.678 0 0 1-.443 1.042C9.613 8.088 8.963 8.5 8 8.5c-.963 0-1.613-.412-2.006-.958A2.679 2.679 0 0 1 5.551 6.5H0v6A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-6z" />
            </svg>
            <span class="nav__name">Payslip</span>
          </a>
        </div>
      </div>

      <a href="logout.php" class="nav__link">
        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-box-arrow-left" viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z" />
          <path fill-rule="evenodd" d="M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z" />
        </svg>
        <span class="nav__name">Log Out</span>
      </a>
    </nav>
  </div>
  <!--CONTENT-->
  <div class="main" id="main">

    <div class="row">
      <div class="col-lg-4">
        <div class="card mb-4">
          <div class="card-body text-center">

<img src="upload/<?php echo $image; ?>" width = 125 height = 125 title="<?php echo $image; ?>">


           <!---<svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
              <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
              <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
            </svg>--->
            <h5 class="my-3"><?php echo $user_data['firstname'] . " " . $user_data['lastname'] ?></h5>
            <p class="text-muted mb-1"><b>ROLE:</b> <?php echo $user_data['role']; ?></p>
            <p>ID: <?php echo $user_data['empId'] ?></p>
            <div class="d-flex justify-content-center mb-2">

              <a href="editprofile.php">
                <button type="button" class="btn btn-outline-primary ms-1" data-target="#myModal">Edit Profile</button>
              </a>
              <button id="modal-btn" class="btn btn-outline-primary ms-1">Profile Image</button>
              <div id="my-modal" class="modal">
        <div class="modal-content"  style="width: 70%">
            <div class="modal-header">
                <span class="close">&times;</span>
                <h2>Update Profile Picture</h2>
            </div>
            <div class="modal-body">
              <div>
                <?php if (isset($_GET['error'])): ?>
    <p><?php echo $_GET['error']; ?></p>
  <?php endif ?>
     <form class="form" id = "form" action="" enctype="multipart/form-data" method="post">
        <div class="upload">
          <?php
          $id = $user_data["id"];
          $name = $user_data["firstname"];
          $image = $user_data["image"];
          ?>
          <img src="upload/<?php echo $image; ?>" width = 125 height = 125 title="<?php echo $image; ?>">
          <div class="round">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <input type="hidden" name="name" value="<?php echo $name; ?>">
            <input type="file" name="image" id = "image" accept=".jpg, .jpeg, .png">
            <i class = "fa fa-camera" style = "color: #fff;"></i>
          </div>
        </div>
      </form>
              </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div><script src="modal.js"></script>
            </div>
          </div>
        </div>

        <div class="col-lg-12">
          <div class="card mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">GSIS Number</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $user_data['gsis']; ?></p>
                </div>
              </div>
     
              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">TIN</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $user_data['tin']; ?></p>
                </div>
              </div>

              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">SSS</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $user_data['sss']; ?></p>
                </div>
              </div>
              <hr>

              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">PhilHealth</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $user_data['philhealth']; ?></p>
                </div>
              </div>

              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">Pag-Ibig</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $user_data['pagibig']; ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="col-lg-8">
        <div class="card mb-4">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">First Name</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0"><?php echo $user_data['firstname'] ?></p>
              </div>
            </div>

            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Middle Name</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0"><?php echo $user_data['middlename'] ?></p>
              </div>
            </div>

            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Last Name</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0"><?php echo $user_data['lastname'] ?></p>
              </div>
            </div>

            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Date of Birth</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0"><?php echo $user_data['birthdate'] ?></p>
              </div>
            </div>

            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Age</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0"><?php echo $user_data['age'] ?></p>
              </div>
            </div>

            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Gender</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0"><?php echo $user_data['gender'] ?></p>
              </div>
            </div>

            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Marital Status</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0"><?php echo $user_data['maritalstatus'] ?></p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Email Address</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0"><?php echo $user_data['email'] ?></p>
              </div>
            </div>
            <hr>

            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Phone</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0"><?php echo $user_data['phone'] ?></p>
              </div>
            </div>
            <hr>

            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Mobile</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0"><?php echo $user_data['mobile'] ?></p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Address</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0"><?php echo $user_data['address'] ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  <!--===== MAIN JS =====-->
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
      document.getElementById("image").onchange = function(){
          document.getElementById("form").submit();
      };
    </script>

</body>

</html>