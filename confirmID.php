<?php

ob_start();
session_start();

include("conn/connection.php");
include("conn/functions.php");

$user_data = check_login($con);

// import email class
require_once dirname(__FILE__) . '/classes/EmailClass.php';
//start import of twilio
require_once __DIR__ . '\classes\twilio-php-main\src\Twilio\autoload.php';
use Twilio\Rest\Client; //This part uses the Client class under twilio
$sid    = "AC28b3232b238fce9a2167f0eceda76098"; //Twilio account SID
$token  = "ead904d3a10f3d5569335c0010a8ae3b";  //Twilio account Token.
$twilio = new Client($sid, $token); //declare twilio variable as new client for later use
?>

<!DOCTYPE html>
<html>

<head>
    <title>Set New Password</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
</head>

<body>
    <div class="bg"></div>
    <div class="loginbox">
        <img src="img/avatars.png" class="avatar" />
        <h1>Set New Password</h1>
        <form method="post" autocomplete="off">
            <p>New Password</p>
            <input id="id_password" type="password" name="newpassword" placeholder="Enter New Password" value="" required />
            <i class="far fa-eye" id="togglePassword" style="margin-left: -30px; cursor: pointer;"></i>
            <p>Confirm Password</p>
            <input id="id_password2" type="password" name="confirmpassword" placeholder="Confirm Password" value="" required />
            <i class="far fa-eye" id="togglePassword2" style="margin-left: -30px; cursor: pointer;"></i>
            <p>EmployeeID</p>
            <input type="text" name="empId" placeholder="Confirm EmployeeID" required />
            <input type="submit" name="" value="Submit" />
            <a href="logout.php">Log In</a><br />
        </form>
    </div>
    <?php

    if ($_SERVER['REQUEST_METHOD'] == "POST") {

        $email = $user_data['email'];
        $empId = $user_data['empId'];

        if ($_POST['newpassword'] != $_POST['confirmpassword']) {
            echo '<script>alert("Password confirmation did not match!")</script>';
        } else if ($_POST['empId'] != $empId) {
            echo '<script>alert("Employee ID did not match!")</script>';
        } else {
            echo '<script>alert("Password updated successfully!")</script>';
            $query = "UPDATE `profile` SET `password` = '" . $_POST['newpassword'] . "' WHERE `empId` = '" . $_POST['empId'] . "'";
            mysqli_query($con, $query);


            //-------------------- email notification ---------------------------

            $data = array(
                'recipient' => $email,
                'subject'   => 'DFCAMCLP SATELLITE CAMPUS - FORGOT PASSWORD',
                'body'      => 'Your password has been changed!'
            );

            $email_class  = new EmailClass($data);

            $notification = $email_class->notification();
            //---- This section starts the twilio SMS API ----
            $message = $twilio->messages->create( //Create a message and use the $twilio variable we set above
                '+639152857371', // Recipient of SMS notification
                [
                'from' => '+19498326678', // Twilio number provided from Twilio.com
                'body' => 'sample sms from sign up' // duh
                ]
            );
            //---- This section ends ----
            //-------------------- end of email notification --------------------

            die;
        }
    }

    ?>
    <script src="script/script.js"></script>
    <script src="script/script2.js"></script>
</body>
</head>

</html>