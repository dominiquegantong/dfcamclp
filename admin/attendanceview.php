<?php

ob_start();
session_start();

include("conn/connection.php");
include("conn/functions.php");

$user_data = check_login($con);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
    <title>Attendance</title>
    <style>
        html {
            font-family: arial;
        }

        body {
            margin: 0;
            position: relative;
        }

        header {
            padding: 10px 10px 10px 15px;
            background-color: #f7f6fb;
            overflow: hidden;
        }

        button {
            background-color: #212529;
            width: 80px;
            height: 40px;
            border-style: none;
            border-radius: 5px;
            cursor: pointer;
            color: white;
        }

        button:hover {
            opacity: 0.7;
        }

        .container {
            padding: 20px;
        }

        .name-card {
            border-radius: 9px;
            width: 400px;
            padding: 20px;
            text-align: center;
            box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);
            margin: 0 auto;
            border-style: solid;
            border-width: 1px;
            border-color: lightgrey;
        }

        .request {
            border-radius: 9px;
            padding: 20px;
            width: 80%;
            margin: 0 auto;
            box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);
            border-style: solid;
            border-width: 1px;
            border-color: lightgrey;
        }

        p {
            font-size: 40px;
        }

        .table {
            height: 550px;
            overflow: auto;
        }

        table {
            border: 1px solid #ddd;
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            table-layout: fixed;
        }

        th,
        td {
            padding: 10px;
            border: 1px solid #ddd;
            font-size: 20px;
            text-align: left;
            overflow: hidden;
        }
    </style>
</head>

<body>
    <header>
        <a href="attendance.php"><button>Back</button></a>
    </header>

    <?php

    // check GET request employee id (empId) parameter
    if (isset($_GET['empId'])) {

        $id = mysqli_real_escape_string($con, $_GET['empId']);

        // make sql
        $sql = "SELECT * FROM `attendance` WHERE empId = $id";

        // get the query result
        $result = mysqli_query($con, $sql);

        // fetch result in array format
        $user_data = mysqli_fetch_assoc($result);

        mysqli_free_result($result);
    }

    ?>

    <div class="container">
        <?php if ($user_data) : ?>
            <div class="name-card">
                <h1><?php echo $user_data['firstname'] . " " . $user_data['lastname'] ?></h1>
                <hr>
                <h3>Instructor</h3>
                <h3><?php echo $user_data['empId'] ?></h3>
            </div>
            <br>
            <div class="request">
                <?php
                $empId = $user_data['empId'];

                // write query for all requests
                $sql = "SELECT * FROM `attendance` WHERE empId = $empId";

                // make query and get result
                $result = mysqli_query($con, $sql);

                // fetch the resulting rows as an array
                $clock = mysqli_fetch_all($result, MYSQLI_ASSOC);

                // free result from memory
                mysqli_free_result($result);

                ?>

                <div class="table">
                    <div class="table-body">
                        <div class="table-border">
                            <p style="text-align: center;">Attendance</p>
                            <table>
                                <thead>
                                    <tr>
                                        <th scope="col">Employee ID</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Time In</th>
                                        <th scope="col">Time Out</th>
                                        <th scope="col">Hours In</th>
                                    </tr>
                                </thead>
                                <?php foreach ($clock as $attendance) : ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $attendance['empId'] ?></td>
                                            <td><?php echo $attendance['firstname'] . " " . $attendance['lastname'] ?></td>
                                            <td><?php echo $attendance['date'] ?></td>
                                            <td><?php echo $attendance['clock_in'] ?></td>
                                            <td><?php echo $attendance['clock_out'] ?></td>
                                            <td><?php echo $attendance['total_time'] ?></td>

                                        </tr>
                                    </tbody>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>

            <h2>No record!</h2>

        <?php endif; ?>
    </div>
</body>

</html>