<?php

ob_start();
session_start();

include("../conn/connection.php");
include("../conn/functions.php");

$user_data = check_login($con);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>DFCAMCLP DATABASE</title>
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #212529;
            -webkit-user-select: none;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 14px;
            text-decoration: none;
        }

        li a:hover:not(.active) {
            background-color: #f7f6fb;
            color: #212529;
            transition: 0.3s;
        }

        .active {
            background-color: #f7f6fb;
            color: #212529;
        }

        p {
            font-size: 40px;
        }

        .table-wrapper {
            overflow-x: auto;
            max-width: 100%;
            margin-left: 8%;
            margin-right: 8%;
            border-color: lightgrey;
        }

        table {
            border: 1px solid #ddd;
            border-collapse: collapse;
            border-spacing: 0;
            width: 160%;
            margin-left: auto;
            margin-right: auto;
        }

        th,
        td {
            padding: 5px;
            border: 1px solid #ddd;
            font-size: 20px;
            text-align: center;
            overflow: auto;
        }

        input {
            text-align: center;
        }

        .confirmation-button {
            padding: 10px;
            text-align: center;
        }

        .btn {
            width: 10%;
        }
    </style>
</head>

<body>

    <ul>
        <li><a href="../index.php">Home</a></li>
        <li><a class="" href="profile.php">Profile</a></li>
        <li><a href="attendance.php">Attendance</a></li>
        <li><a href="payslip.php">Payslip</a></li>
        <li><a href="leave_request.php">Leave Request</a></li>
        <li><a href="approved_leave.php">Approved Leave</a></li>
        <li><a href="declined_leave.php">Declined Leave</a></li>
        <li><a href="sick_leave.php">Sick Leave</a></li>
        <li><a href="vacation_leave.php">Vacation Leave</a></li>
        <li><a href="spl.php">Special Privilege Leave</a></li>
        <li style="float: right;"><a class="active" disabled><b>DATABASE SECTION</b></a></li>
    </ul>

    <br>
    <p style="text-align: center;">Profile</p>
    <hr>
    <?php

    $empId = $_GET["empId"];

    // write query for all requests
    $sql = "SELECT * FROM `profile` WHERE `empId` = $empId";

    // make query and get result
    $result = mysqli_query($con, $sql);

    // fetch the resulting rows as an array
    $profile = mysqli_fetch_all($result, MYSQLI_ASSOC);;

    ?>

    <h1 style="text-align: center;">Are you sure you want to delete this user data?</h1>
    <hr style="width: 85%; margin: auto;">
    <br>
    <form method="post">
        <div class="table-wrapper">
            <div class="table">
                <div class="table-body">
                    <table>
                        <thead>
                            <tr>
                                <th scope="col">Employee ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Role</th>
                                <th scope="col">Date of Birth</th>
                                <th scope="col">Age</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Marital Status</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Mobile</th>
                                <th scope="col">Email</th>
                                <th scope="col">Password</th>
                            </tr>
                        </thead>
                        <?php foreach ($profile as $user) : ?>
                            <tbody>
                                <tr>
                                    <td><?php echo $user['empId'] ?></td>
                                    <td><?php echo $user['firstname'] . " " . $user['middlename'] . " " . $user['lastname'] ?></td>
                                    <td><?php echo $user['role'] ?></td>
                                    <td><?php echo $user['birthdate'] ?></td>
                                    <td><?php echo $user['age'] ?></td>
                                    <td><?php echo $user['gender'] ?></td>
                                    <td><?php echo $user['maritalstatus'] ?></td>
                                    <td><?php echo $user['phone'] ?></td>
                                    <td><?php echo $user['mobile'] ?></td>
                                    <td><?php echo $user['email'] ?></td>
                                    <td><?php echo $user['password'] ?></td>
                                </tr>
                            </tbody>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
        <br>
        <hr style="width: 85%; margin: auto;">
        <br>
        <div class="confirmation-button">
            <input type="submit" class="btn btn-danger" name="delete" value="Delete">
            <input type="submit" class="btn btn-primary" name="cancel" value="Cancel">
        </div>
    </form>

    <?php

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $empId = $_GET['empId'];

        $query = "SELECT * FROM `profile` ORDER BY id";
        mysqli_query($con, $query);

        if (isset($_POST['delete'])) {
            $query1 = "DELETE FROM `profile` WHERE `profile`.`empId` = $empId";
            $updateResult = mysqli_query($con, $query1);
            header("Location: profile.php");
        }

        if (isset($_POST['cancel'])) {
            header("Location: profile.php");
        }
    }

    ?>
</body>

</html>