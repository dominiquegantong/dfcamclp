<?php

session_start();

include("../conn/connection.php");
include("../conn/functions.php");

$user_data = check_login($con);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>DFCAMCLP DATABASE</title>
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #212529;
            -webkit-user-select: none;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 14px;
            text-decoration: none;
        }

        li a:hover:not(.active) {
            background-color: #f7f6fb;
            color: #212529;
            transition: 0.3s;
        }

        .active {
            background-color: #f7f6fb;
            color: #212529;
            pointer-events: none;

        }

        .table {
            height: 720px;
            overflow: auto;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }

        p {
            font-size: 40px;
        }

        table {
            border: 1px solid #ddd;
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            table-layout: fixed;
        }

        th,
        td {
            padding: 5px;
            border: 1px solid #ddd;
            font-size: 20px;
            text-align: center;
            overflow: auto;
        }
    </style>
</head>

<body>

    <ul>
        <li><a href="../index.php">Home</a></li>
        <li><a href="profile.php">Profile</a></li>
        <li><a href="attendance.php">Attendance</a></li>
        <li><a href="payslip.php">Payslip</a></li>
        <li><a href="leave_request.php">Leave Request</a></li>
        <li><a href="approved_leave.php">Approved Leave</a></li>
        <li><a href="declined_leave.php">Declined Leave</a></li>
        <li><a href="sick_leave.php">Sick Leave</a></li>
        <li><a class="active" href="vacation_leave.php">Vacation Leave</a></li>
        <li><a href="spl.php">Special Privilege Leave</a></li>
        <li style="float: right;"><a class="active" disabled><b>DATABASE SECTION</b></a></li>
    </ul>

    <br>
    <p style="text-align: center;">Vacation Leave</p>
    <hr>
    <div class="container">
        <?php

        // write query for all requests
        $sql = "SELECT * FROM `vacation_leave` ORDER BY id";

        // make query and get result
        $result = mysqli_query($con, $sql);

        // fetch the resulting rows as an array
        $vacation_leave = mysqli_fetch_all($result, MYSQLI_ASSOC);

        // free result from memory
        mysqli_free_result($result);

        ?>

        <div class="table">
            <div class="table-body">
                <div class="table-border">
                    <table>
                        <thead>
                            <tr>
                                <th scope="col">Employee ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Date</th>

                            </tr>
                        </thead>
                        <?php foreach ($vacation_leave as $vl) : ?>
                            <tbody>
                                <tr>
                                    <td><?php echo $vl['empId'] ?></td>
                                    <td><?php echo $vl['firstname'] . " " . $vl['lastname'] ?></td>
                                    <td><?php echo $vl['date'] ?></td>
                                </tr>
                            </tbody>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

</body>

</html>