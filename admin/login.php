<?php

session_start();

include("conn/connection.php");
include("conn/functions.php");

?>

<!DOCTYPE html>
<html>

<head>
  <title>Login</title>
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
</head>

<body>
  <div class="bg"></div>
  <div class="loginbox">
    <img src="img/avatars.png" class="avatar" />
    <h1>ADMIN LOG IN</h1>
    
    <form method="post">
      <p>Email</p>
      <input type="text" name="email" placeholder="Enter Email" autocomplete="off" />
      <p>Password</p>
      <input id="id_password" type="password" name="password" placeholder="Enter Password" />
      <i class="far fa-eye" id="togglePassword" style="margin-left: -30px; cursor: pointer;"></i>
      <input type="submit" name="" value="Log In" />
    </form>
    
  </div>
  <?php

  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    //something was posted
    $email = $_POST['email'];
    $password = $_POST['password'];

    //read from database
    $query = "select * from admin where email = '$email' limit 1";
    $result = mysqli_query($con, $query);

    if ($result) {
      if ($result && mysqli_num_rows($result) > 0) {

        $user_data = mysqli_fetch_assoc($result);

        if ($user_data['password'] === $password) {

          $_SESSION['id'] = $user_data['id'];
          header("Location: index.php");
          die;
        }
      }
    }

    if (!empty($email) || !empty($password)) {
      echo '<script>alert("Email or Password is incorrect");</script>';
    }
  }

  ?>
  <script src="script/script.js"></script>
</body>
</head>

</html>