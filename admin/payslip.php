<?php

ob_start();
session_start();

include("conn/connection.php");
include("conn/functions.php");

$user_data = check_login($con);

// import email class
require_once dirname(dirname(__FILE__)) . '../classes/EmailClass.php';
//start import of twilio
require_once __DIR__ . '..\classes\twilio-php-main\src\Twilio\autoload.php';
use Twilio\Rest\Client; //This part uses the Client class under twilio
$sid    = "AC28b3232b238fce9a2167f0eceda76098"; //Twilio account SID
$token  = "ead904d3a10f3d5569335c0010a8ae3b";  //Twilio account Token.
$twilio = new Client($sid, $token); //declare twilio variable as new client for later use
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!--BOX ICONS-->
  <link href="https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css" rel="stylesheet" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />

  <!--CSS-->
  <link rel="stylesheet" href="assets/css/styles.css" />

  <!-- JavaScript Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <title>DFCAMCLP Admin Portal</title>

  <style>
    .container {
      width: 100%;
      margin: 0 auto;
    }

    .tab {
      overflow: hidden;
      border: 1px solid #ccc;
      background-color: #f1f1f1;

    }

    .tab button {
      background-color: inherit;
      float: left;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 14px 16px;
      transition: 0.3s;
      font-size: 17px;
    }

    .tab button:hover {
      background-color: #ddd;
    }

    .tab button.active {
      background-color: #ccc;
    }

    .tabcontent {
      padding: 6px 12px;
      border: 1px solid #ccc;
      border-top: none;

    }

    label {
      padding-right: 20px;
    }

    .btn {
      margin: 10px;
      padding: 5px 25px 5px 25px;

    }

    table {
      background-color: #f2f4f7;
    }

    .year,
    .month,
    .day,
    .button {
      margin: 10px;
    }

    input {
      text-align: center;
    }

    h2 {
      text-align: center;
    }

    .submit {
      float: right;
    }
  </style>
</head>


<body id="body-pd">
  <header class="header" id="header">
    <div class="header__toggle">
      <i class="bx bx-menu" id="header-toggle"></i>
    </div>

    <!--PROFILE ICON-->
    <div class="header__img">
      <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
        <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
        <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
      </svg>
    </div>
  </header>

  <div class="l-navbar" id="nav-bar">
    <nav class="nav">
      <div>
        <a href="index.php" class="nav__logo">
          <i class="bx bx-layer nav__logo-icon"></i>
          <span class="nav__logo-name">DFCAMCLP Admin<br>Portal</span>
        </a>

        <div class="nav__list">
          <a href="index.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-house-door" viewBox="0 0 16 16">
              <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z" />
            </svg>
            <span class="nav__name">Home</span>
          </a>

          <a href="attendance.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-calendar-date" viewBox="0 0 16 16">
              <path d="M6.445 11.688V6.354h-.633A12.6 12.6 0 0 0 4.5 7.16v.695c.375-.257.969-.62 1.258-.777h.012v4.61h.675zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871 0-1.934-.781-2.668-1.953-2.668-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82h-.684zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23z" />
              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
            </svg>
            <span class="nav__name">Attendance</span>
          </a>

          <a href="leave.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-calendar-event" viewBox="0 0 16 16">
              <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z" />
              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
            </svg>
            <span class="nav__name">Leave</span>
          </a>

          <a href="#" class="nav__link active">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-wallet-fill" viewBox="0 0 16 16">
              <path d="M1.5 2A1.5 1.5 0 0 0 0 3.5v2h6a.5.5 0 0 1 .5.5c0 .253.08.644.306.958.207.288.557.542 1.194.542.637 0 .987-.254 1.194-.542.226-.314.306-.705.306-.958a.5.5 0 0 1 .5-.5h6v-2A1.5 1.5 0 0 0 14.5 2h-13z" />
              <path d="M16 6.5h-5.551a2.678 2.678 0 0 1-.443 1.042C9.613 8.088 8.963 8.5 8 8.5c-.963 0-1.613-.412-2.006-.958A2.679 2.679 0 0 1 5.551 6.5H0v6A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-6z" />
            </svg>
            <span class="nav__name">Payslip</span>
          </a>

          <a href="database.php" class="nav__link">
          <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-database" viewBox="0 0 16 16">
                    <path d="M4.318 2.687C5.234 2.271 6.536 2 8 2s2.766.27 3.682.687C12.644 3.125 13 3.627 13 4c0 .374-.356.875-1.318 1.313C10.766 5.729 9.464 6 8 6s-2.766-.27-3.682-.687C3.356 4.875 3 4.373 3 4c0-.374.356-.875 1.318-1.313ZM13 5.698V7c0 .374-.356.875-1.318 1.313C10.766 8.729 9.464 9 8 9s-2.766-.27-3.682-.687C3.356 7.875 3 7.373 3 7V5.698c.271.202.58.378.904.525C4.978 6.711 6.427 7 8 7s3.022-.289 4.096-.777A4.92 4.92 0 0 0 13 5.698ZM14 4c0-1.007-.875-1.755-1.904-2.223C11.022 1.289 9.573 1 8 1s-3.022.289-4.096.777C2.875 2.245 2 2.993 2 4v9c0 1.007.875 1.755 1.904 2.223C4.978 15.71 6.427 16 8 16s3.022-.289 4.096-.777C13.125 14.755 14 14.007 14 13V4Zm-1 4.698V10c0 .374-.356.875-1.318 1.313C10.766 11.729 9.464 12 8 12s-2.766-.27-3.682-.687C3.356 10.875 3 10.373 3 10V8.698c.271.202.58.378.904.525C4.978 9.71 6.427 10 8 10s3.022-.289 4.096-.777A4.92 4.92 0 0 0 13 8.698Zm0 3V13c0 .374-.356.875-1.318 1.313C10.766 14.729 9.464 15 8 15s-2.766-.27-3.682-.687C3.356 13.875 3 13.373 3 13v-1.302c.271.202.58.378.904.525C4.978 12.71 6.427 13 8 13s3.022-.289 4.096-.777c.324-.147.633-.323.904-.525Z"/>
                    </svg>
            <span class="nav__name">Database</span>
          </a>
        </div>
      </div>

      <a href="logout.php" class="nav__link">
        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-box-arrow-left" viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z" />
          <path fill-rule="evenodd" d="M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z" />
        </svg>
        <span class="nav__name">Log Out</span>
      </a>
    </nav>
  </div>
  <!---tabs--->
  <h2 style="padding: 10px;"><b>PAYSLIPS<b></h2>

  <!-- PAYSLIP -->

  <?php

  // check GET request id parameter
  if (isset($_GET['id'])) {

    $id = mysqli_real_escape_string($con, $_GET['id']);

    // make sql
    $sql = "SELECT * FROM profile WHERE id = $id";

    // get the query result
    $result = mysqli_query($con, $sql);

    // fetch result in array format
    $user_data = mysqli_fetch_assoc($result);

    mysqli_free_result($result);
  }

  ?>

  <div class="col-8 container">
    <?php if ($user_data) : ?>
      <form method="post">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col"><img src="img/logo.png" height=100 width=100 style="display: block;  margin-left: auto; margin-right: auto;"></th>
              <th scope="col" colspan="3">
                <h2 style="text-align: center; padding:10px;">DR. FILEMON C. AGUILAR MEMORIAL COLLEGE OF LAS PIÑAS</h2>
              </th>

            </tr>
          </thead>

          <tbody style="text-align: center;">
            <tr class="table-secondary">
              <td colspan="4">PAYSLIP FOR THE MONTH OF OCTOBER</td>
            </tr>
            <tr>
              <td width="25%">Full Name:</td>
              <td width="25%"><input type="text" name="fullname" value="<?php echo $user_data['firstname'] . " " . $user_data['lastname'] ?>" readonly></td>
              <td width=" 25%">Employee ID:</td>
              <td width="25%"><input type="text" name="empId" value="<?php echo $user_data['empId'] ?>"></td>
            </tr>
          </tbody>
        </table>

        <?php

        $basic = 13000;
        $pera = 2000;
        $gsis = 1000;
        $philhealth = 500;
        $pagibig = 100;
        $tax = 350;

        ?>

        <table class="table table-bordered">
          <tbody style="text-align: center;">

            <tr>
              <th>EARNINGS</td>
              <th>AMOUNT</td>
              <th>DEDUCTIONS</td>
              <th>AMOUNT</td>
            </tr>
            <tr>
              <td width="25%">BASIC</td>
              <td width="25%"><input type="text" name="basic" value="<?php echo $basic ?>" readonly></td>
              <td width="25%">GSIS Contribution</td>
              <td width="25%"><input type="text" name="d1" value="<?php echo $gsis ?>" readonly></td>
            </tr>
            <tr>
              <td width="25%">OT</td>
              <td width="25%"><input type="text" name="ot" autocomplete="off" required></td>
              <td width="25%">Philhealth Contribution</td>
              <td width="25%"><input type="text" name="d2" value="<?php echo $philhealth ?>" readonly></td>
            </tr>
          <tr>
          <td width="25%">PERA</td>
              <td width="25%"><input type="text" name="pera" value="<?php echo $pera ?>" readonly></td>
              <td width="25%">PAGIBIG Contribution</td>
              <td width="25%"><input type="text" name="d3" value="<?php echo $pagibig ?>" readonly></td>
            </tr>
            <tr>
            <td></td>
              <td></td>
              <td width="25%">Withholding Tax</td>
              <td width="25%"><input type="text" name="d4" value="<?php echo $tax ?>" readonly></td>
            </tr>
            <tr>
              
              <td width="25%">Gross Pay</td>
              <td width="25%">--</td>
              <td width="25%">Other deductions</td>
              <td width="25%"><input type="text" name="d5" autocomplete="off" required></td>
            </tr>


            <tr class="table-secondary">
              <td width="25%">Net Pay</td>
              <td colspan="3"><input type="text" name="netpay" readonly></td>
            </tr>
          </tbody>
        </table>
        <input type="submit" class="submit" name="submit" value="Submit Payslip">
      </form>
    <?php else : ?>

      <h5>No such user exists!</h5>

    <?php endif; ?>
  </div>

  <?php

  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    // something was posted
    $empId = $_POST['empId'];
    $basic = $_POST['basic'];
    $pera = $_POST['pera'];
    $ot = $_POST['ot'];
    $d1 = $_POST['d1'];
    $d2 = $_POST['d2'];
    $d3 = $_POST['d3'];
    $d4 = $_POST['d4'];
    $d5 = $_POST['d5'];

    $grosspay = ($basic + $ot + $pera);
    $netpay = (($grosspay) - ($d1 + $d2 + $d3 + $d4 + $d5));

    if (isset($_POST['submit'])) {

      $appUpdateQuery = "DELETE FROM payslip WHERE `payslip`.`empId` = $empId";
      $appUpdateResult = mysqli_query($con, $appUpdateQuery);
      $sql = "INSERT INTO payslip (empId,basic,pera,total_time,ot,grosspay,netpay,deduction1,deduction2,deduction3,deduction4,deduction5) VALUES ('$empId','$basic','$pera','$totalTime','$ot','$grosspay','$netpay','$d1','$d2','$d3','$d4','$d5')";
      mysqli_query($con, $sql);

      //-------------------- email notification ---------------------------

      $data = array(
        'recipient' => $user_data['email'],
        'subject'   => 'DFCAMCLP SATELLITE CAMPUS - PAYSLIP',
        'body'      => "
        
        <h3>YOUR MONTHLY PAYSLIP IS ALREADY GENERATED</h3>
        
        <table style='border-style: solid; border-width: 1px; border-radius: 3px; border-collapse: collapse;'>
          <tbody style='text-align: center;'>
            <tr>
              <th style='text-align: center;' colspan='4'>DR. FILEMON C. AGUILAR MEMORIAL COLLEGE OF LAS PIÑAS</td>
            </tr>
            <tr>
              <th style='border-style: solid; border-width: 1px;'>EARNINGS</td>
              <th style='border-style: solid; border-width: 1px;'>AMOUNT</td>
              <th style='border-style: solid; border-width: 1px;'>DEDUCTIONS</td>
              <th style='border-style: solid; border-width: 1px;'>AMOUNT</td>
            </tr>
            <tr>
              <td style='border-style: solid; border-width: 1px;' width='25%'>BASIC</td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>$basic</td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>GSIS Contribution</td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>$d1</td>
            </tr>
            <tr>
              <td style='border-style: solid; border-width: 1px;' width='25%'>OT</td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>$ot</td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>Philhealth Contribution</td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>$d2</td>
            </tr>
            <tr>
            <td style='border-style: solid; border-width: 1px;' width='25%'>PERA</td>
            <td style='border-style: solid; border-width: 1px;' width='25%'>$pera</td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>PAGIBIG Contribution</td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>$d3</td>
            </tr>
            <tr>
           <td>
           <td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>Withholding Tax</td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>$d4</td>
            </tr>
            <tr>
              <td style='border-style: solid; border-width: 1px;' width='25%'>Gross Pay</td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>$grosspay</td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>Other deductions</td>
              <td style='border-style: solid; border-width: 1px;' width='25%'>$d5</td>
            </tr>
            <tr class='table-secondary'>
              <td style='border-style: solid; border-width: 1px;' width='25%'>Net Pay</td>
              <td style='border-style: solid; border-width: 1px;' colspan='3'>$netpay</td>
            </tr>
          </tbody>
      </table>
      
      "
      );

      $email_class  = new EmailClass($data);

      $notification = $email_class->notification();
      //---- This section starts the twilio SMS API ----
      $message = $twilio->messages->create( //Create a message and use the $twilio variable we set above
        '+639152857371', // Recipient of SMS notification
        [
          'from' => '+19498326678', // Twilio number provided from Twilio.com
          'body' => 'sample sms from sign up' // duh
        ]
      );
      //---- This section ends ----
      //-------------------- end of email notification --------------------

      header('Location: index.php');
    }
  }

  ?>

  <script>
    function year(evt, taon) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(taon).style.display = "block";
      evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
  </script>
  <script src="assets/js/main.js"></script>
</body>

</html>