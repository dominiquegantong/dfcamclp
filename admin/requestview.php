<?php

ob_start();
session_start();

include("conn/connection.php");
include("conn/functions.php");

$user_data = check_login($con);
require_once dirname(dirname(__FILE__)).'../classes/EmailClass.php';
//start import of twilio
require_once __DIR__ . '..\classes\twilio-php-main\src\Twilio\autoload.php';
use Twilio\Rest\Client; //This part uses the Client class under twilio
$sid    = "AC28b3232b238fce9a2167f0eceda76098"; //Twilio account SID
$token  = "ead904d3a10f3d5569335c0010a8ae3b";  //Twilio account Token.
$twilio = new Client($sid, $token); //declare twilio variable as new client for later use
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pending Requests</title>
    <style>
        html {
            font-family: arial;
        }

        body {
            margin: 0;
            position: relative;
        }

        header {
            padding: 10px 10px 10px 15px;
            background-color: #f7f6fb;
            overflow: hidden;
        }

        button {
            background-color: #212529;
            width: 80px;
            height: 40px;
            border-style: none;
            border-radius: 5px;
            cursor: pointer;
            color: white;
        }

        button:hover {
            opacity: 0.7;
        }

        .container {
            padding: 20px;
        }

        .name-card {
            border-radius: 9px;
            width: 400px;
            padding: 20px;
            text-align: center;
            box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);
            margin: 0 auto;
            border-style: solid;
            border-width: 1px;
            border-color: lightgrey;
        }

        .request {
            border-radius: 9px;
            padding: 20px;
            width: 80%;
            margin: 0 auto;
            box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);
            border-style: solid;
            border-width: 1px;
            border-color: lightgrey;
        }

        p {
            font-size: 40px;
        }

        table {
            border: 1px solid #ddd;
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            table-layout: fixed;
        }

        th,
        td {
            padding: 10px;
            border: 1px solid #ddd;
            font-size: 20px;
            text-align: left;
            overflow: hidden;
        }

        input {
            font-size: 20px;
        }

        .btn-approve,
        .btn-decline {
            height: 40px;
            border-style: none;
            border-radius: 5px;
            cursor: pointer;
            color: white;
            text-align: center;
            margin: 5px;
        }

        .btn-approve {
            background-color: limegreen;
        }

        .btn-decline {
            background-color: red;
        }

        .btn-approve:hover,
        .btn-decline:hover {
            opacity: 0.7;
        }
    </style>
</head>

<body>
    <header>
        <a href="leave.php"><button>Back</button></a>
    </header>

    <?php

    // check GET request employee id (empId) parameter
    if (isset($_GET['empId'])) {

        $id = mysqli_real_escape_string($con, $_GET['empId']);

        // make sql
        $sql = "SELECT * FROM profile WHERE empId = $id";

        // get the query result
        $result = mysqli_query($con, $sql);

        // fetch result in array format
        $user_data = mysqli_fetch_assoc($result);

        mysqli_free_result($result);
    }

    ?>

    <div class="container">
        <?php if ($user_data) : ?>
            <div class="name-card">
                <h1><?php echo $user_data['firstname'] . " " . $user_data['lastname'] ?></h1>
                <h3><?php echo $user_data['role'] ?></h3>
                <h3><?php echo $user_data['empId'] ?></h3>
            </div>
            <br>
            <div class="request">
                <?php
                $empId = $user_data['empId'];

                // write query for all requests
                $sql = "select * from leaverequest where empId = $empId order by date";

                // make query and get result
                $result = mysqli_query($con, $sql);

                ?>

                <div class="table">
                    <div class="table-body">
                        <div class="table-border">
                            <form method="post">
                                <p style="text-align: center;">Pending Leave Request</p>
                                <table>
                                    <thead>
                                        <tr>
                                            <th scope="col">Employee ID</th>
                                            <th scope="col">Leave Type</th>
                                            <th scope="col">Leave Date</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <?php while ($request = mysqli_fetch_array($result)) {
                                        echo '<tbody>';
                                        echo '<tr>';
                                        echo '<td><input type="text" value="' . $request['empId'] . '"style="border-style: none;" name="empId" readonly></td>';
                                        echo '<td><input type="text" value="' . $request['leaveType'] . '" style="border-style: none;" name="leaveType" readonly></td>';
                                        echo '<td><input type="text" value="' . $request['date'] . '" style="border-style: none;" name="date" readonly></td>';
                                        echo '<td><input type="text" value="' . $request['status'] . '" style="border-style: none;" name="status" readonly></td>';
                                        echo '<td style="text-align: center;"><input type="submit" class="btn-approve" name="approve" value="Approve"> / <input type="submit" class="btn-decline" name="decline" value="Decline"></td>';
                                        echo '</tr>';
                                        echo '</tbody>';
                                    }
                                    ?>
                                </table>
                            </form>
                            <?php

                            $sick = "SELECT * FROM `sick_leave` ORDER BY id";
                            $vacation = "SELECT * FROM `vacation_leave` ORDER BY id";
                            $spl = "SELECT * FROM `special_privilege_leave` ORDER BY id";

                            $result1 = mysqli_query($con, $sick);
                            $result2 = mysqli_query($con, $vacation);
                            $result3 = mysqli_query($con, $spl);

                            $userdata1 = mysqli_fetch_assoc($result1);
                            $userdata2 = mysqli_fetch_assoc($result2);
                            $userdata3 = mysqli_fetch_assoc($result3);

                            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                                $query = "SELECT * FROM leaverequest ORDER BY id";
                                mysqli_query($con, $query);

                                $empId = $_POST['empId'];
                                $fname = $user_data['firstname'];
                                $lname = $user_data['lastname'];
                                $type = $_POST['leaveType'];
                                $date = $_POST['date'];


                                $approve = "APPROVED";
                                $decline = "DECLINED";
                                $email = $user_data['email'];
                                if (isset($_POST['approve'])) {
                                    $appUpdateQuery = "DELETE FROM leaverequest WHERE `leaverequest`.`empId` = $empId";
                                    $appUpdateResult = mysqli_query($con, $appUpdateQuery);
                                    $appUpdateQuery2 = "INSERT INTO `approved` (empId,firstname,lastname,leaveType,date,status) VALUES ('$empId','$fname','$lname','$type','$date','$approve')";
                                    $appUpdateResult2 = mysqli_query($con, $appUpdateQuery2);

                                    //-------------------- email notification ---------------------------

                                    $data = array(
                                       
                                        'recipient' => $email,
                                        'subject'   => "DFCAMCLP SATELLITE CAMPUS LEAVE REQUEST FOR: $fname $lname!",
                                        'body'      => "<p>Your leave request was accepted!</p>"
                                    );

                                    $email_class  = new EmailClass($data);

                                    $notification = $email_class->notification();
                                    //---- This section starts the twilio SMS API ----
                                    //---- This section starts the twilio SMS API ----
                                    $message = $twilio->messages->create( //Create a message and use the $twilio variable we set above
                                        '+639152857371', // Recipient of SMS notification
                                        [
                                        'from' => '+19498326678', // Twilio number provided from Twilio.com
                                        'body' => 'sample sms from sign up' // duh
                                        ]
                                    );
                                    //---- This section ends ----
                                    //-------------------- end of email notification --------------------

                                    header('Location: leave.php');

                                    if ($type === 'Sick Leave') {
                                        $sickLeave = "INSERT INTO `sick_leave` (empId,firstname,lastname,date) VALUES ('$empId','$fname','$lname','$date')";
                                        $insertIntoSickLeave = mysqli_query($con, $sickLeave);
                                        header('Location: leave.php');
                                    } else if ($type === 'Vacation Leave') {
                                        $vacationLeave = "INSERT INTO `vacation_leave` (empId,firstname,lastname,date) VALUES ('$empId','$fname','$lname','$date')";
                                        $insertIntoVacationLeave = mysqli_query($con, $vacationLeave);
                                        header('Location: leave.php');
                                    } else if ($type === 'Special Privilege Leave') {
                                        $SPL = "INSERT INTO `special_privilege_leave` (empId,firstname,lastname,date) VALUES ('$empId','$fname','$lname','$date')";
                                        $insertIntoSPL = mysqli_query($con, $SPL);
                                        header('Location: leave.php');
                                    }
                                }


                                if (isset($_POST['decline'])) {
                                    $appUpdateQuery = "DELETE FROM leaverequest WHERE `leaverequest`.`empId` = $empId";
                                    $appUpdateResult = mysqli_query($con, $appUpdateQuery);
                                    $appUpdateQuery2 = "INSERT INTO `declined` (empId,firstname,lastname,leaveType,date,status) values ('$empId','$fname','$lname','$type','$date','$decline')";
                                    $appUpdateResult2 = mysqli_query($con, $appUpdateQuery2);

                                    //-------------------- email notification ---------------------------

                                    $data = array(
                                        'recipient' => $email,
                                        'subject'   => "DFCAMCLP SATELLITE CAMPUS LEAVE REQUEST FOR: $fname $lname!",
                                        'body'      => "<p>Your leave request was declined!</p>"
                                    );

                                    $email_class  = new EmailClass($data);

                                    $notification = $email_class->notification();

                                    //-------------------- end of email notification --------------------

                                    header('Location: leave.php');
                                }
                            }

                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>

            <h2>No such user exists!</h2>

        <?php endif; ?>
    </div>
</body>

</html>