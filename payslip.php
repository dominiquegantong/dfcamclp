<?php

session_start();

include("conn/connection.php");
include("conn/functions.php");

$user_data = check_login($con);

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!--BOX ICONS-->
  <link href="https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css" rel="stylesheet" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />

  <!--CSS-->
  <link rel="stylesheet" href="assets/css/styles.css" />

  <!-- JavaScript Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.5/dist/html2canvas.min.js">
  </script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <title>DFCAMCLP Employee Portal</title>

  <style>
    .tab {
      overflow: hidden;
      border: 1px solid #ccc;
      background-color: #f1f1f1;

    }

    .tab button {
      background-color: inherit;
      float: left;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 14px 16px;
      transition: 0.3s;
      font-size: 17px;
    }

    .tab button:hover {
      background-color: #ddd;
    }

    .tab button.active {
      background-color: #ccc;
    }

    .tabcontent {
      padding: 6px 12px;
      border: 1px solid #ccc;
      border-top: none;

    }

    label {
      padding-right: 20px;
    }

    .btn {
      margin: 10px;
      padding: 5px 25px 5px 25px;

    }

    table {
      background-color: #f2f4f7;
    }

    .year,
    .month,
    .day,
    .button {
      margin: 10px;
    }

    #photo {
      margin-top: 5%;
    }
  </style>
</head>


<body id="body-pd">
  <header class="header" id="header">
    <div class="header__toggle">
      <i class="bx bx-menu" id="header-toggle"></i>
    </div>
    <!--SEARCH BAR
        <div class="input-group">
        <div class="form-outline">
        <input id="search-input" type="search" id="form1" class="form-control" />
            <label class="form-label" for="form1">Search</label>
        </div>
        <button id="search-button" type="button" class="btn btn-primary">
        <i class="fas fa-search"></i>
        </button>
        </div>
        -->

    <!--PROFILE ICON-->
    <div class="header__img">
      <?php echo "<b>" . $user_data['firstname'] . " " . $user_data['lastname'] . "</b>" ?>
      <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
        <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
        <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
      </svg>
    </div>
  </header>

  <div class="l-navbar" id="nav-bar">
    <nav class="nav">
      <div>
        <a href="index.php" class="nav__logo">
          <i class="bx bx-layer nav__logo-icon"></i>
          <span class="nav__logo-name">DFCAMCLP Portal</span>
        </a>

        <div class="nav__list">
          <a href="index.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-house-door" viewBox="0 0 16 16">
              <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z" />
            </svg>
            <span class="nav__name">Home</span>
          </a>

          <a href="profile.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
              <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
            </svg>
            <span class="nav__name">Profile</span>
          </a>

          <a href="attendance.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-calendar-date" viewBox="0 0 16 16">
              <path d="M6.445 11.688V6.354h-.633A12.6 12.6 0 0 0 4.5 7.16v.695c.375-.257.969-.62 1.258-.777h.012v4.61h.675zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871 0-1.934-.781-2.668-1.953-2.668-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82h-.684zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23z" />
              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
            </svg>
            <span class="nav__name">Attendance</span>
          </a>

          <a href="leave.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-calendar-event" viewBox="0 0 16 16">
              <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z" />
              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
            </svg>
            <span class="nav__name">Leave</span>
          </a>

          <a href="#" class="nav__link active">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-wallet-fill" viewBox="0 0 16 16">
              <path d="M1.5 2A1.5 1.5 0 0 0 0 3.5v2h6a.5.5 0 0 1 .5.5c0 .253.08.644.306.958.207.288.557.542 1.194.542.637 0 .987-.254 1.194-.542.226-.314.306-.705.306-.958a.5.5 0 0 1 .5-.5h6v-2A1.5 1.5 0 0 0 14.5 2h-13z" />
              <path d="M16 6.5h-5.551a2.678 2.678 0 0 1-.443 1.042C9.613 8.088 8.963 8.5 8 8.5c-.963 0-1.613-.412-2.006-.958A2.679 2.679 0 0 1 5.551 6.5H0v6A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-6z" />
            </svg>
            <span class="nav__name">Payslip</span>
          </a>
        </div>
      </div>

      <a href="logout.php" class="nav__link">
        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-box-arrow-left" viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z" />
          <path fill-rule="evenodd" d="M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z" />
        </svg>
        <span class="nav__name">Log Out</span>
      </a>
    </nav>
  </div>
  <!---tabs--->


  <div id="photo">
    <div class="row-8">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col"><img src="img/logo.png" height=100 width=100 style="display: block;  margin-left: auto; margin-right: auto;"></th>
            <th scope="col" colspan="3">
              <h2 style="text-align: center; padding:10px;">DR. FILEMON C. AGUILAR MEMORIAL COLLEGE OF LAS PIÑAS</h2>
            </th>

          </tr>
        </thead>
        <tbody style="text-align: center;">
          <tr class="table-secondary">
            <td colspan="4">PAYSLIP FOR THE MONTH OF <?php echo strtoupper(date("F")); ?></td>
          </tr>
          <tr>
            <td width="25%">Full Name:</td>
            <td width="25%"><?php echo $user_data['firstname'] . " " . $user_data['middlename'] . " " . $user_data['lastname'] ?></td>
            <td width="25%">Employee ID:</td>
            <td width="25%"><?php echo $user_data['empId'] ?></td>
          </tr>
          <tr>
            <td width="25%">TIN</td>
            <td width="25%"><?php echo $user_data['tin'] ?></td>
            <td width="25%">GSIS</td>
            <td width="25%"><?php echo $user_data['gsis'] ?></td>
          </tr>
          <tr>
            <td width="25%">Philhealth</td>
            <td width="25%"><?php echo $user_data['philhealth'] ?></td>
            <td width="25%">PAGIBIG</td>
            <td width="25%"><?php echo $user_data['pagibig'] ?></td>
          </tr>
        </tbody>
      </table>

      <?php

      $empId = $user_data['empId'];

      // write query for all requests
      $sql = "SELECT * FROM payslip WHERE empId = $empId";

      // make query and get result
      $result = mysqli_query($con, $sql);

      // fetch the resulting rows as an array
      $user_data = mysqli_fetch_all($result, MYSQLI_ASSOC);

      // free result from memory
      mysqli_free_result($result);

      // close connection

      ?>

      <table class="table table-bordered">
        <?php foreach ($user_data as $data) : ?>
          <tbody style="text-align: center;">

            <tr>
              <th>EARNINGS</td>
              <th>AMOUNT</td>
              <th>DEDUCTIONS</td>
              <th>AMOUNT</td>
            </tr>
            <tr>
              <td width="25%">BASIC</td>
              <td width="25%"><?php echo $data['basic'] ?></td>
              <td width="25%">GSIS Contribution</td>
              <td width="25%"><?php echo $data['deduction1'] ?></td>
            </tr>
            <tr>
              <td width="25%">OT</td>
              <td width="25%"><?php echo $data['ot'] ?></td>
              <td width="25%">Philhealth Contribution</td>
              <td width="25%"><?php echo $data['deduction2'] ?></td>
            </tr>
            <tr>
            <td width="25%">PERA</td>
              <td width="25%"><?php echo $data['pera'] ?></td>
              <td width="25%">PAGIBIG Contribution</td>
              <td width="25%"><?php echo $data['deduction3'] ?></td>
            </tr>
            <tr>
            <td></td>
            <td></td>
              <td width="25%">Withholding Tax</td>
              <td width="25%"><?php echo $data['deduction4'] ?></td>
            </tr>
            <tr>
              <td width="25%">Gross Pay</td>
              <td width="25%"><?php echo $data['grosspay'] ?></td>
              <td width="25%">Other deductions</td>
              <td width="25%"><?php echo $data['deduction5'] ?></td>
            </tr>


            <tr class="table-secondary">
              <td width="25%">Net Pay</td>
              <td colspan="3"><?php echo $data['netpay'] ?></td>
            </tr>
          </tbody>
      </table>
    <?php endforeach; ?>

    <button class="btn" onclick="takeshot(); this.disabled=true;"><i class="fa fa-download"></i>Download</button>
    <div id="output"></div>

    <script type="text/javascript">
      // Define the function 
      // to screenshot the div
      function takeshot() {
        let div =
          document.getElementById('photo');

        // Use the html2canvas
        // function to take a screenshot
        // and append it
        // to the output div
        html2canvas(div).then(
          function(canvas) {
            document.getElementById('output').appendChild(canvas);
          })

      }
    </script>
    </div>
  </div>
  </div>






  <script>
    function year(evt, taon) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(taon).style.display = "block";
      evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
  </script>
  <script src="assets/js/main.js"></script>
</body>

</html>