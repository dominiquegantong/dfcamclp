<?php

ob_start();
session_start();

include("conn/connection.php");
include("conn/functions.php");

$user_data = check_login($con);

?>

<!DOCTYPE html>
<html>

<head>
    <title>Set New Password</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
</head>

<body>
    <div class="bg"></div>
    <div class="loginbox">
        <img src="img/avatars.png" class="avatar" />
        <h1>Set New Password</h1>
        <form method="post" onsubmit="return alert('Password updated successfully!')" autocomplete="off">
            <p>New Password</p>
            <input id="id_password" type="password" name="newpassword" placeholder="Enter New Password" value="" required />
            <i class="far fa-eye" id="togglePassword" style="margin-left: -30px; cursor: pointer;"></i>
            <p>Confirm Password</p>
            <input id="id_password2" type="password" name="confirmpassword" placeholder="Confirm Password" value="" required />
            <i class="far fa-eye" id="togglePassword2" style="margin-left: -30px; cursor: pointer;"></i>
            <input type="submit" name="" value="Submit" />
            <a href="profile.php">Back to Profile</a><br />
        </form>
    </div>
    <?php

    if ($_SERVER['REQUEST_METHOD'] == "POST") {

        $id = $user_data['id'];

        if ($_POST['newpassword'] != $_POST['confirmpassword']) {
            echo '<script>alert("Password confirmation did not match!")</script>';
        } else {
            sleep(3);
            $query = "UPDATE `profile` SET `password` = '" . $_POST['newpassword'] . "' WHERE `id` = $id";
            mysqli_query($con, $query);
            header("Location: login.php");
            die;
        }
    }

    ?>
    <script src="script/script.js"></script>
    <script src="script/script2.js"></script>
</body>
</head>

</html>