<?php

class HttpRequestClass
{
    public function __construct($config = null)
    {
        $this->email_base_url = 'https://wedyb.com/';
    }

    public function get($url, $data = [], $header = [])
    {
        $res = new stdClass;

        $option['ssl']['verify_peer']      = FALSE;
        $option['ssl']['verify_peer_name'] = FALSE;

        $option['http']['method']          = 'GET';
        $option['http']['header'][0]       = isset($header[0]) ? $header[0] : 'Content-Type: application/json';
        $option['http']['header'][1]       = isset($header[1]) ? $header[1] : 'Authorization: Bearer ';

        if ($option['http']['header'][0] == 'Content-Type: application/json') {
            $content = json_encode($data);
        }

        if ($option['http']['header'][0] == 'Content-Type: application/xml') {
            $content = $data;
        }

        if ($option['http']['header'][0] == 'application/x-www-form-urlencoded') {
            $content = http_build_query($data);
        }

        $option['http']['content']       = $content;
        $option['http']['ignore_errors'] = TRUE;

        $use_include_path = FALSE;
        $context          = stream_context_create($option);

        // send request & get response
        $http_response = file_get_contents($url, $use_include_path, $context);
        $status_code   = (int) filter_var(trim($http_response_header[0], 'HTTP/1.1'), FILTER_SANITIZE_NUMBER_INT);

        $res->data        = json_decode($http_response);
        $res->status_code = $status_code;

        return $res;
    }

    public function post($url, $data = [], $header = [])
    {
        $res = new stdClass;

        $option['ssl']['verify_peer']      = FALSE;
        $option['ssl']['verify_peer_name'] = FALSE;

        $option['http']['method']          = 'POST';
        $option['http']['header'][0]       = isset($header[0]) ? $header[0] : 'Content-Type: application/json';
        $option['http']['header'][1]       = isset($header[1]) ? $header[1] : 'Authorization: Bearer ';

        if ($option['http']['header'][0] == 'Content-Type: application/json') {
            $content = json_encode($data);
        }

        if ($option['http']['header'][0] == 'Content-Type: application/xml') {
            $content = $data;
        }

        if ($option['http']['header'][0] == 'application/x-www-form-urlencoded') {
            $content = http_build_query($data);
        }

        $option['http']['content']       = $content;
        $option['http']['ignore_errors'] = TRUE;

        $use_include_path = FALSE;
        $context          = stream_context_create($option);

        // send request & get response
        $http_response = file_get_contents($url, $use_include_path, $context);
        $status_code   = (int) filter_var(trim($http_response_header[0], 'HTTP/1.1'), FILTER_SANITIZE_NUMBER_INT);

        $res->data        = json_decode($http_response) ? json_decode($http_response) : $http_response;
        $res->status_code = $status_code;

        return $res;
    }
}
