<?php

require_once dirname(__FILE__) . '/HttpRequestClass.php';

class EmailClass
{
    public function __construct($user_info = null)
    {
        $this->user_info = $user_info;

        // classes
        $this->http = new HttpRequestClass();
    }

    public function notification()
    {
        $res = (object) array();

        $send  = $this->http->post(
            $url    = $this->http->email_base_url . 'app/api/email/notification',
            $data   = $this->user_info,
            $header = ['Content-Type: application/json', 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX3R5cGUiOiJpbnRlcm5hbCIsInVzZXJuYW1lIjoiaW50ZXJuYWwiLCJ1c2VyX2lkIjoyNiwiZW52IjoiZGV2ZWxvcG1lbnQiLCJpYXQiOjE2Njg5MTU4MjksIm5iZiI6MTY2ODkxNTgyOSwiZXhwIjoxNjcxNTA3ODI5fQ.vWSVVZu3MnFCz4PKwMxix7cyPJSqEyXuqTuIMfZ4qTI']
        );

        if ($send->status_code == 200) {
            $res->status  = 'success';
            $res->message = '';
            $res->data    = $send->data;
        } else {
            $res->status  = 'error';
            $res->message = 'from ' . $url;
            $res->data    = [$send->data];
        }

        return $res;
    }
}
