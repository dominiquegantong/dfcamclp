<?php

ob_start();
session_start();

include("conn/connection.php");
include("conn/functions.php");

$user_data = check_login($con);

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!--BOX ICONS-->
  <link href="https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css" rel="stylesheet" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />

  <!--CSS-->
  <link rel="stylesheet" href="assets/css/styles.css" />

  <title>DFCAMCLP Employee Portal</title>
</head>
<style>
  .main {
    text-align: center;
    padding: 20px;
  }

  .clock {
    color: #212529;
    font-size: 60px;
    letter-spacing: 7px;
  }

  .card-body-a {
    padding: 20px;
    border-radius: 10px;
    border-style: solid;
    border-width: 1px;
    border-color: lightgrey;
    overflow: auto;
    height: 600px;
    box-shadow: 0 3px 5px rgb(0 0 0 / 0.2);
  }

  h2 {
    padding: 10px;
  }
</style>

<body id="body-pd">
  <header class="header" id="header">
    <div class="header__toggle">
      <i class="bx bx-menu" id="header-toggle"></i>
    </div>
    <div class="header__img">
      <?php echo "<b>" . $user_data['firstname'] . " " . $user_data['lastname'] . "</b>" ?>
      <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
        <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
        <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
      </svg>
    </div>
  </header>

  <div class="l-navbar" id="nav-bar">
    <nav class="nav">
      <div>
        <a href="index.php" class="nav__logo">
          <i class="bx bx-layer nav__logo-icon"></i>
          <span class="nav__logo-name">DFCAMCLP Portal</span>
        </a>

        <div class="nav__list">
          <a href="index.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-house-door" viewBox="0 0 16 16">
              <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z" />
            </svg>
            <span class="nav__name">Home</span>
          </a>

          <a href="profile.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
              <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
            </svg>
            <span class="nav__name">Profile</span>
          </a>

          <a href="#" class="nav__link active">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-calendar-date" viewBox="0 0 16 16">
              <path d="M6.445 11.688V6.354h-.633A12.6 12.6 0 0 0 4.5 7.16v.695c.375-.257.969-.62 1.258-.777h.012v4.61h.675zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871 0-1.934-.781-2.668-1.953-2.668-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82h-.684zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23z" />
              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
            </svg>
            <span class="nav__name">Attendance</span>
          </a>

          <a href="leave.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-calendar-event" viewBox="0 0 16 16">
              <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z" />
              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
            </svg>
            <span class="nav__name">Leave</span>
          </a>

          <a href="payslip.php" class="nav__link">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-wallet-fill" viewBox="0 0 16 16">
              <path d="M1.5 2A1.5 1.5 0 0 0 0 3.5v2h6a.5.5 0 0 1 .5.5c0 .253.08.644.306.958.207.288.557.542 1.194.542.637 0 .987-.254 1.194-.542.226-.314.306-.705.306-.958a.5.5 0 0 1 .5-.5h6v-2A1.5 1.5 0 0 0 14.5 2h-13z" />
              <path d="M16 6.5h-5.551a2.678 2.678 0 0 1-.443 1.042C9.613 8.088 8.963 8.5 8 8.5c-.963 0-1.613-.412-2.006-.958A2.679 2.679 0 0 1 5.551 6.5H0v6A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-6z" />
            </svg>
            <span class="nav__name">Payslip</span>
          </a>
        </div>
      </div>

      <a href="logout.php" class="nav__link">
        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-box-arrow-left" viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z" />
          <path fill-rule="evenodd" d="M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z" />
        </svg>
        <span class="nav__name">Log Out</span>
      </a>
    </nav>
  </div>

  <?php

  $empId = $user_data['empId'];

  // write query for all requests
  $sql = "SELECT * FROM `attendance` WHERE empId = $empId";

  // make query and get result
  $result = mysqli_query($con, $sql);

  // fetch the resulting rows as an array
  $attendance = mysqli_fetch_all($result, MYSQLI_ASSOC);

  // free result from memory
  mysqli_free_result($result);

  // close connection

  ?>

  <div class="main">

    <div id="MyClockDisplay" name="clock" class="clock" onload="showTime()">
    </div>

    <div class="row">
      <div class="col">
        <form id="form" method="post">
          <div class=" card-body">
            <h2><input id="btnClockIn" onclick="func()" type="submit" name="submitIn" style="float: left;" class="btn btn-outline-primary ms-1" value="Punch">CLOCK-IN</h2>
          </div>
          <br>
          <div class=" col">
            <div class="card-body-a">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th scope="col">Employee ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Role</th>
                    <th scope="col">Date</th>
                    <th scope="col">Time In</th>
                  </tr>
                </thead>
                <?php foreach ($attendance as $clock_in) : ?>
                  <tbody>
                    <tr>
                      <td>
                        <p><?php echo $clock_in['empId'] ?></p>
                      </td>
                      <td>
                        <p><?php echo $clock_in['firstname'] . " " . $clock_in['lastname'] ?></p>
                      </td>
                      <td>
                        <p><?php echo $clock_in['role'] ?></p>
                      </td>
                      <td>
                        <p><?php echo $clock_in['date'] ?></p>
                      </td>
                      <td>
                        <p><?php echo $clock_in['clock_in'] ?></p>
                      </td>
                    </tr>
                  </tbody>
                <?php endforeach; ?>
              </table>
            </div>
          </div>
        </form>
      </div>
      <div class="col">
        <form method="post">
          <div class="card-body">
            <h2><input id="btnClockOut" type="submit" name="submitOut" style="float: left;" class="btn btn-outline-primary ms-1" value="Punch">CLOCK-OUT</h2>
          </div>
          <br>
          <div class="col">
            <div class="card-body-a">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th scope="col">Employee ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Role</th>
                    <th scope="col">Date</th>
                    <th scope="col">Time Out</th>
                  </tr>
                </thead>
                <?php foreach ($attendance as $clock_out) : ?>
                  <tbody>
                    <tr>
                      <td>
                        <p><?php echo $clock_out['empId'] ?></p>
                      </td>
                      <td>
                        <p><?php echo $clock_out['firstname'] . " " . $clock_out['lastname'] ?></p>
                      </td>
                      <td>
                        <p><?php echo $clock_in['role'] ?></p>
                      </td>
                      <td>
                        <p><?php echo $clock_out['date'] ?></p>
                      </td>
                      <td>
                        <p><?php echo $clock_out['clock_out'] ?></p>
                      </td>
                    </tr>
                  </tbody>
                <?php endforeach; ?>
              </table>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <?php

  if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $empId = $user_data['empId'];
    $firstname = $user_data['firstname'];
    $lastname = $user_data['lastname'];
    $date = date("Y-m-d");
    date_default_timezone_set('Asia/Manila');
    $timeIn = date("g:iA");
    $timeOut = date("g:iA");
    $role = $user_data['role'];


    if (isset($_POST['submitIn'])) {
      $clockInQuery = "INSERT INTO `attendance` (`empId`, `firstname`, `lastname`, `role`, `date`, `clock_in`) VALUES ('$empId', '$firstname','$lastname', '$role', '$date','$timeIn')";
      mysqli_real_query($con, $clockInQuery);
    }

    if (isset($_POST['submitOut'])) {
      $clockOutQuery = "UPDATE `attendance` SET `clock_out` = '$timeOut' WHERE `clock_out` = '' AND empId = '$empId'";
      mysqli_query($con, $clockOutQuery);

      $query = "SELECT `clock_in` FROM `attendance` ORDER BY `id` DESC LIMIT 1";
      $result = mysqli_query($con, $query);
      $stamp1 = mysqli_fetch_assoc($result);

      $query2 = "SELECT `clock_out` FROM `attendance` ORDER BY `id` DESC LIMIT 1";
      $result2 = mysqli_query($con, $query2);
      $stamp2 = mysqli_fetch_assoc($result2);

      $time1 = strtotime($stamp1['clock_in']);
      $time2 = strtotime($stamp2['clock_out']);
      $total_time = round(abs($time2 - $time1) / (60 * 60), 2);

      $clockOutQuery1 = "UPDATE `attendance` SET `total_time` = '$total_time' WHERE `total_time` = '' AND empId = '$empId'";
      mysqli_query($con, $clockOutQuery1);

      header("Location: attendance.php");
    }
  }
  ?>

  </div>

</body>

</html>
<!--===== MAIN JS =====-->
<script src="assets/js/main.js"></script>
<script>
  function showTime() {
    var date = new Date();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    var session = "AM";
    if (h == 0) {
      h = 12;
    }
    if (h > 12) {
      hh = h - 12;
      session = "PM";
    }
    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;
    var time = h + ":" + m + ":" + s + " " + session;
    document.getElementById("MyClockDisplay").innerText = time;
    document.getElementById("MyClockDisplay").textContent = time;
    setTimeout(showTime, 1000);
  }
  showTime();
</script>

</body>

</html>